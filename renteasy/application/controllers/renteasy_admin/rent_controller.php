<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class rent_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
               $this->login_validate();


        $data['count'] = $this->db->count_all('property_rent');
        $query = $this->db->query('SELECT * FROM property_rent;');
        $data['property'] = $query->result();
          if(!isset($_SESSION)){
            session_start();
        }
        $_SESSION['admin_page']="rent";
        $this->load->view('admin/header_view');
        $this->load->view('admin/navigation_view');
        $this->load->view('admin/rent_view', $data);
        $this->load->view('admin/footer_view');
    }

    public function rent_process() {
                       $this->login_validate();
  $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $property_title_en=$_POST['property_title_en'];
        $property_title_ch=$_POST['property_title_ch'];
        $address = $_POST['address'];
        $year = $_POST['year'];
        $month = $_POST['month'];
        $day = $_POST['day'];
        $bedroom = $_POST['bedroom'];
        $bathroom = $_POST['bathroom'];
        $garage = $_POST['garage'];
        $price = $_POST['price'];
        $suburb = $_POST['suburb'];
        $bus = $_POST['bus'];
        $tram = $_POST['tram'];
        $train = $_POST['train'];
        $others_ch = $_POST['others_ch'];
        $others_en = $_POST['others_en'];
        $facility_nearby_ch=$_POST['facility_nearby_ch'];
        $facility_nearby_en=$_POST['facility_nearby_en'];
        $uni = (isset($_POST['rmit']) ? 'RMIT:' : "") .
                (isset($_POST['mel_uni']) ? 'mel_uni:' : "") .
                (isset($_POST['monash_uni']) ? 'monash_uni:' : "") .
                (isset($_POST['latrobe_uni']) ? 'latrobe_uni:' : "") .
                (isset($_POST['swinburne_uni']) ? 'swinburne_uni:' : "")
        ;


        $data = array(
            'address' => $address,
            'available_date' => $year . '-' . $month . '-' . $day,
            'status' => 'available',
            'bedroom' => $bedroom,
            'bathroom' => $bathroom,
            'garage' => $garage,
            'price' => $price,
            'uni_nearby' => $uni,
            'bus_nearby' => $bus,
            'tram_nearby' => $tram,
            'train_station_nearby' => $train,
            'others_en' => $others_en,
            'others_ch' => $others_ch,
            'suburb' => $suburb,
            'facility_nearby_ch'=>$facility_nearby_ch,
            'facility_nearby_en'=>$facility_nearby_en,
            'property_title_ch'=>$property_title_ch,
            'property_title_en'=>$property_title_en,
               'longitude' => $longitude,
            'latitude' => $latitude
        );
        $this->db->insert('property_rent', $data);
        //get the last index

        $query = $this->db->query('SELECT MAX(property_no) as ppo from property_rent;');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $num = $row->ppo;
        } else {
            $num = 0;
        }

        $this->process_image($num);
        redirect(base_url() . 'index.php/renteasy_admin/rent_controller/index');
    }

    public function process_image($num) {
                       $this->login_validate();

        $dir = 'upload_files/rent_images/';
        $t = 'i1';
        for ($i = 1; $i < 5; $i++) {
            if ($_FILES["i$i"]['error'] != UPLOAD_ERR_OK) {
                switch ($_FILES["i$i"]['error']) {
                    case UPLOAD_ERR_INI_SIZE: //其值为 1，上传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值
                        die('The upload file exceeds the upload_max_filesize directive in php.ini');
                        break;
                    case UPLOAD_ERR_FORM_SIZE: //其值为 2，上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值
                        die('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
                        break;
                    case UPLOAD_ERR_PARTIAL: //其值为 3，文件只有部分被上传
                        die('The uploaded file was only partially uploaded.');
                        break;
                    case UPLOAD_ERR_NO_FILE: //其值为 4，没有文件被上传
                        die('No file was uploaded.');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR: //其值为 6，找不到临时文件夹
                        die('The server is missing a temporary folder.');
                        break;
                    case UPLOAD_ERR_CANT_WRITE: //其值为 7，文件写入失败
                        die('The server failed to write the uploaded file to disk.');
                        break;
                    case UPLOAD_ERR_EXTENSION: //其他异常
                        die('File upload stopped by extension.');
                        break;
                }
            }


            list($width, $height, $type, $attr) = getimagesize($_FILES["i$i"]['tmp_name']);

            switch ($type) {
                case IMAGETYPE_GIF:
                    $image = imagecreatefromgif($_FILES["i$i"]['tmp_name']) or die('The file you upload was not supported filetype');
                    $ext = '.gif';
                    break;
                case IMAGETYPE_JPEG:
                    $image = imagecreatefromjpeg($_FILES["i$i"]['tmp_name']) or die('The file you upload was not supported filetype');
                    $ext = '.jpg';
                    break;
                case IMAGETYPE_PNG:
                    $image = imagecreatefrompng($_FILES["i$i"]['tmp_name']) or die('The file you upload was not supported filetype');
                    $ext = '.png';
                    break;
                default:
                    die('The file you uploaded was not a supported filetype.');
            }

            $imagename = "rent_" . $num . "_" . $i . $ext;

            switch ($type) {
                case IMAGETYPE_GIF:
                    imagegif($image, $dir . '/' . $imagename);
                    break;
                case IMAGETYPE_JPEG:
                    imagejpeg($image, $dir . '/' . $imagename);
                    break;
                case IMAGETYPE_PNG:
                    imagepng($image, $dir . '/' . $imagename);
                    break;
            }
            imagedestroy($image);
        }
    }

    public function delete($number) {
                       $this->login_validate();

        $array = array(
            'property_no' => $number);

        $this->db->delete('property_rent', $array);
        redirect(base_url() . 'index.php/renteasy_admin/rent_controller/index');
    }

    public function update($number) {
                       $this->login_validate();

        $query = $this->db->query('SELECT * from property_rent Where property_no=' . $number . ";");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data['longitude']=$row->longitude;
            $data['latitude']=$row->latitude;
           
            $data['property_no'] = $row->property_no;
            $data['address'] = $row->address;
            list($year, $month, $day) = preg_split('[\-]', $row->available_date);
            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['status'] = $row->status;
            $data['bedroom'] = $row->bedroom;
            $data['bathroom'] = $row->bathroom;
            $data['garage'] = $row->garage;
            $data['price'] = $row->price;
            $data['uni_nearby'] = $row->uni_nearby;
            $data['bus_nearby'] = $row->bus_nearby;
            $data['tram_nearby'] = $row->tram_nearby;
            $data['train_station_nearby'] = $row->train_station_nearby;
            $data['others_en'] = $row->others_en;
            $data['others_ch'] = $row->others_ch;
            $data['suburb'] = $row->suburb;
            $data['uni_nearby'] = $row->uni_nearby;
            $data['facility_nearby_ch']=$row->facility_nearby_ch;
            $data['facility_nearby_en']=$row->facility_nearby_en;
            $data['property_title_ch']=$row->property_title_ch;
            $data['property_title_en']=$row->property_title_en;
        }
        $this->load->view('admin/header_view');
        $this->load->view('admin/navigation_view');
        $this->load->view('admin/rentitem_view', $data);
        $this->load->view('admin/footer_view');
    }

    public function rent_update() {
                       $this->login_validate();
$latitude=$_POST['latitude'];
        $longitude=$_POST['longitude'];
 $property_title_en=$_POST['property_title_en'];
        $property_title_ch=$_POST['property_title_ch'];
        $property_no = $_POST['property_no'];
        $address = $_POST['address'];
        $year = $_POST['year'];
        $month = $_POST['month'];
        $day = $_POST['day'];
        $bedroom = $_POST['bedroom'];
        $bathroom = $_POST['bathroom'];
        $garage = $_POST['garage'];
        $price = $_POST['price'];
        $suburb = $_POST['suburb'];
        $bus = $_POST['bus'];
        $tram = $_POST['tram'];
        $train = $_POST['train'];
        $others_ch = $_POST['others_ch'];
        $others_en = $_POST['others_en'];
        $facility_nearby_ch=$_POST['facility_nearby_ch'];
        $facility_nearby_en=$_POST['facility_nearby_en'];
        $uni = (isset($_POST['rmit']) ? 'RMIT:' : "") .
                (isset($_POST['mel_uni']) ? 'mel_uni:' : "") .
                (isset($_POST['monash_uni']) ? 'monash_uni:' : "") .
                (isset($_POST['latrobe_uni']) ? 'latrobe_uni:' : "") .
                (isset($_POST['swinburne_uni']) ? 'swinburne_uni:' : "")
        ;


        $data = array(
//            'property_no' => $property_no,
            'address' => $address,
            'available_date' => $year . '-' . $month . '-' . $day,
            'status' => 'available',
            'bedroom' => $bedroom,
            'bathroom' => $bathroom,
            'garage' => $garage,
            'price' => $price,
            'uni_nearby' => $uni,
            'bus_nearby' => $bus,
            'tram_nearby' => $tram,
            'train_station_nearby' => $train,
            'others_en' => $others_en,
            'others_ch' => $others_ch,
            'suburb' => $suburb,
            'facility_nearby_ch'=>$facility_nearby_ch,
            'facility_nearby_en'=>$facility_nearby_en,
            'property_title_ch'=>$property_title_ch,
            'property_title_en'=>$property_title_en,
            'latitude'=>$latitude,
            'longitude'=>$longitude
        );
        $this->db->where('property_no', $property_no);
        $this->db->update('property_rent', $data);

        $query = $this->db->query('SELECT MAX(property_no) as ppo from property_rent;');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $num = $row->ppo;
        } else {
            $num = 0;
        }

//        $this->process_image($num);
        redirect(base_url() . 'index.php/renteasy_admin/rent_controller/index');
    }

    public function property_change() {
                       $this->login_validate();

        $property_no = $_POST['property_no'];
        $status = $_POST['status'];
        $this->db->query('UPDATE property_rent SET status=\'' . $status . '\' where property_no=' . $property_no);
        redirect(base_url() . 'index.php/renteasy_admin/rent_controller/index');
    }

    
     public function login_validate() {

        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION['login'])) {
          
        } else {
            redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');
        }
    }
}
