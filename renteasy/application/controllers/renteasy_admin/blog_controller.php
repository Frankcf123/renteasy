<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class blog_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
       $this->login_validate();


        $data['count'] = $this->db->count_all('blog');

        $query = $this->db->query('SELECT * FROM blog;');
        $data['blogs'] = $query->result();
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['admin_page'] = "blog";
        $this->load->view('admin/header_view');
        $this->load->view('admin/navigation_view');

        $this->load->view('admin/blog_view', $data);
        $this->load->view('admin/footer_view');
    }

    public function blog_process() {
               $this->login_validate();

        $blog_title_en = $_POST['blog_title_en'];
        $blog_title_ch = $_POST['blog_title_ch'];
        $blog_content_en = $_POST['blog_content_en'];
        $blog_content_ch = $_POST['blog_content_ch'];
        $blog_tag = $_POST['blog_tag'];

        $data = array(
            'blog_title_en' => $blog_title_en,
            'blog_title_ch' => $blog_title_ch,
            'blog_content_en' => $blog_content_en,
            'blog_content_ch' => $blog_content_ch,
            'blog_tag' => $blog_tag
        );
        $this->db->insert('blog', $data);
        //get the last index

        $query = $this->db->query('SELECT MAX(blog_no) as ppo from blog;');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $num = $row->ppo;
        } else {
            $num = 0;
        }

//        $this->process_image($num);
        redirect(base_url() . 'index.php/renteasy_admin/blog_controller/index');
    }

    public function delete($number) {
               $this->login_validate();

        $array = array(
            'blog_no' => $number);

        $this->db->delete('blog', $array);
        redirect(base_url() . 'index.php/renteasy_admin/blog_controller/index');
    }

    public function update($number) {
               $this->login_validate();

        $query = $this->db->query('SELECT * from blog Where blog_no=' . $number . ";");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data['blog_no'] = $number;
            $data['blog_title_en'] = $row->blog_title_en;
            $data['blog_title_ch'] = $row->blog_title_ch;
            $data['blog_content_en'] = $row->blog_content_en;
            $data['blog_content_ch'] = $row->blog_content_ch;
            $data['blog_tag'] = $row->blog_tag;
        }
        $this->load->view('admin/header_view');
        $this->load->view('admin/navigation_view');
        $this->load->view('admin/blogitem_view', $data);
        $this->load->view('admin/footer_view');
    }

    public function blog_update() {
               $this->login_validate();

$blog_no=$_POST['blog_no'];
        $blog_title_en = $_POST['blog_title_en'];
        $blog_title_ch = $_POST['blog_title_ch'];
        $blog_content_en = $_POST['blog_content_en'];
        $blog_content_ch = $_POST['blog_content_ch'];
        $blog_tag = $_POST['blog_tag'];

        $data = array(
            'blog_title_en' => $blog_title_en,
            'blog_title_ch' => $blog_title_ch,
            'blog_content_en' => $blog_content_en,
            'blog_content_ch' => $blog_content_ch,
            'blog_tag' => $blog_tag
        );
        $this->db->where('blog_no', $blog_no);
        $this->db->update('blog', $data);

        $query = $this->db->query('SELECT MAX(blog_no) as ppo from blog;');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $num = $row->ppo;
        } else {
            $num = 0;
        }

//        $this->process_image($num);
        redirect(base_url() . 'index.php/renteasy_admin/blog_controller/index');
    }

     public function login_validate() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION['login'])) {
          
        } else {
            redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');
        }
    }

}
