<?php

class invest_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
               $this->login_validate();


        $data['count'] = $this->db->count_all('property_invest');

        $query = $this->db->query('SELECT * FROM property_invest;');
        $data['property'] = $query->result();
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['admin_page'] = "invest";
        $this->load->view('admin/header_view');
        $this->load->view('admin/navigation_view');
        $this->load->view('admin/invest_view', $data);
        $this->load->view('admin/footer_view');
    }

    public function invest_process() {
               $this->login_validate();
  $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $address = $_POST['address'];
        $year = $_POST['year'];
        $month = $_POST['month'];
        $day = $_POST['day'];
        $property_title_en = $_POST['property_title_en'];
        $property_title_ch = $_POST['property_title_ch'];
        $property_content_en = $_POST['property_content_en'];
        $property_content_ch = $_POST['property_content_ch'];
        $property_tag_en = $_POST['property_tag_en'];
        $property_tag_ch = $_POST['property_tag_ch'];
        $price = $_POST['price'];
        $type = $_POST['type'];
        $data = array(
            'address' => $address,
            'available_date' => $year . '-' . $month . '-' . $day,
            'status' => 'available',
            'price' => $price,
            'property_title_en' => $property_title_en,
            'property_title_ch' => $property_title_ch,
            'property_content_en' => $property_content_en,
            'property_content_ch' => $property_content_ch,
            'property_tag_ch' => $property_tag_ch,
            'property_tag_en' => $property_tag_en,
            'type' => $type,
             'longitude' => $longitude,
            'latitude' => $latitude
        );
        $this->db->insert('property_invest', $data);
        //get the last index

        $query = $this->db->query('SELECT MAX(property_no) as ppo from property_invest;');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $num = $row->ppo;
        } else {
            $num = 0;
        }

        $this->process_image($num);
        redirect(base_url() . 'index.php/renteasy_admin/invest_controller/index');
    }

    public function process_image($num) {
               $this->login_validate();

        $dir = 'upload_files/invest_images/';
        $t = 'i1';
        for ($i = 1; $i < 5; $i++) {
            if ($_FILES["i$i"]['error'] != UPLOAD_ERR_OK) {
                switch ($_FILES["i$i"]['error']) {
                    case UPLOAD_ERR_INI_SIZE: //其值为 1，上传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值
                        die('The upload file exceeds the upload_max_filesize directive in php.ini');
                        break;
                    case UPLOAD_ERR_FORM_SIZE: //其值为 2，上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值
                        die('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
                        break;
                    case UPLOAD_ERR_PARTIAL: //其值为 3，文件只有部分被上传
                        die('The uploaded file was only partially uploaded.');
                        break;
                    case UPLOAD_ERR_NO_FILE: //其值为 4，没有文件被上传
                        die('No file was uploaded.');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR: //其值为 6，找不到临时文件夹
                        die('The server is missing a temporary folder.');
                        break;
                    case UPLOAD_ERR_CANT_WRITE: //其值为 7，文件写入失败
                        die('The server failed to write the uploaded file to disk.');
                        break;
                    case UPLOAD_ERR_EXTENSION: //其他异常
                        die('File upload stopped by extension.');
                        break;
                }
            }


            list($width, $height, $type, $attr) = getimagesize($_FILES["i$i"]['tmp_name']);

            switch ($type) {
                case IMAGETYPE_GIF:
                    $image = imagecreatefromgif($_FILES["i$i"]['tmp_name']) or die('The file you upload was not supported filetype');
                    $ext = '.gif';
                    break;
                case IMAGETYPE_JPEG:
                    $image = imagecreatefromjpeg($_FILES["i$i"]['tmp_name']) or die('The file you upload was not supported filetype');
                    $ext = '.jpg';
                    break;
                case IMAGETYPE_PNG:
                    $image = imagecreatefrompng($_FILES["i$i"]['tmp_name']) or die('The file you upload was not supported filetype');
                    $ext = '.png';
                    break;
                default:
                    die('The file you uploaded was not a supported filetype.');
            }

            $imagename = "invest_" . $num . "_" . $i . $ext;

            switch ($type) {
                case IMAGETYPE_GIF:
                    imagegif($image, $dir . '/' . $imagename);
                    break;
                case IMAGETYPE_JPEG:
                    imagejpeg($image, $dir . '/' . $imagename);
                    break;
                case IMAGETYPE_PNG:
                    imagepng($image, $dir . '/' . $imagename);
                    break;
            }
            imagedestroy($image);
        }
    }

    public function delete($number) {
               $this->login_validate();

        $array = array(
            'property_no' => $number);

        $this->db->delete('property_invest', $array);
        redirect(base_url() . 'index.php/renteasy_admin/invest_controller/index');
    }

    public function update($number) {
               $this->login_validate();

        $query = $this->db->query('SELECT * from property_invest Where property_no=' . $number . ";");
        if ($query->num_rows() > 0) {
            $row = $query->row();
             $data['longitude']=$row->longitude;
            $data['latitude']=$row->latitude;
            $data['property_no'] = $row->property_no;
            $data['address'] = $row->address;

            list($year, $month, $day) = preg_split('[\-]', $row->available_date);
            $data['year'] = $year;
            $data['month'] = $month;
            $data['day'] = $day;
            $data['status'] = $row->status;
            $data['price'] = $row->price;
            $data['property_title_en'] = $row->property_title_en;
            $data['property_title_ch'] = $row->property_title_ch;
            $data['property_content_en'] = $row->property_content_en;
            $data['property_content_ch'] = $row->property_content_ch;
            $data['property_tag_en'] = $row->property_tag_en;
            $data['property_tag_ch'] = $row->property_tag_ch;
            $data['type'] = $row->type;
        }
        $this->load->view('admin/header_view');
        $this->load->view('admin/navigation_view');
        $this->load->view('admin/investitem_view', $data);
        $this->load->view('admin/footer_view');
    }

    public function invest_update() {
           $this->login_validate();
  $latitude=$_POST['latitude'];
        $longitude=$_POST['longitude'];
        $property_no = $_POST['property_no'];
        $address = $_POST['address'];
        $year = $_POST['year'];
        $month = $_POST['month'];
        $day = $_POST['day'];
        $property_title_en = $_POST['property_title_en'];
        $property_title_ch = $_POST['property_title_ch'];
        $property_content_en = $_POST['property_content_en'];
        $property_content_ch = $_POST['property_content_ch'];
        $property_tag_en = $_POST['property_tag_en'];
        $property_tag_ch = $_POST['property_tag_ch'];
        $price = $_POST['price'];
        $type = $_POST['type'];
        $data = array(
            'address' => $address,
            'available_date' => $year . '-' . $month . '-' . $day,
            'status' => 'available',
            'price' => $price,
            'property_title_en' => $property_title_en,
            'property_title_ch' => $property_title_ch,
            'property_content_en' => $property_content_en,
            'property_content_ch' => $property_content_ch,
            'property_tag_ch' => $property_tag_ch,
            'property_tag_en' => $property_tag_en,
            'type' => $type,
            'latitude'=>$latitude,
            'longitude'=>$longitude
        );


        $this->db->where('property_no', $property_no);
        $this->db->update('property_invest', $data);

        $query = $this->db->query('SELECT MAX(property_no) as ppo from property_invest;');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $num = $row->ppo;
        } else {
            $num = 0;
        }

//        $this->process_image($num);
        redirect(base_url() . 'index.php/renteasy_admin/invest_controller/index');
    }

    public function property_change() {
               $this->login_validate();

        $property_no = $_POST['property_no'];
        $status = $_POST['status'];
        $this->db->query('UPDATE property_invest SET status=\'' . $status . '\' where property_no=' . $property_no);
        redirect(base_url() . 'index.php/renteasy_admin/invest_controller/index');
    }

     public function login_validate() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION['login'])) {
          
        } else {
            redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');
        }
    }
}
