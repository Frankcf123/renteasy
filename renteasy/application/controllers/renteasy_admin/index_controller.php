<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class index_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {

        $data['count'] = $this->db->count_all('request');

        $query = $this->db->query('SELECT * FROM request;');
        $data['requests'] = $query->result();
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['lang']='english';
                $this->config->set_item('language', $_SESSION['lang']);

        $_SESSION['admin_page'] = "index";
        if (isset($_SESSION['login'])) {
            $this->load->view('admin/header_view');
            $this->load->view('admin/navigation_view');
            $this->load->view('admin/index_view', $data);
            $this->load->view('admin/footer_view');
        } else {
            $this->login_view();
        }
    }

    public function login_view() {
        $this->load->view('admin/admin_login');
        $this->load->view('modal_view');
    }

    public function login() {
        

        $username = $_POST['username'];
        $pwd = $_POST['password'];
        if ($username == "renteasyadmin" && $pwd == "renteasyadmin123") {
            if (!isset($_SESSION)) {
                session_start();
            }
            $_SESSION['login'] = true;
            redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');
        } else {
            $this->index();
        }
    }

    public function done($request_no) {

        $this->db->query('UPDATE request SET request_status=\'' . 'done' . '\' where request_no=' . $request_no);
        redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');
    }

    public function request_details($request_no) {
        $query = $this->db->query('SELECT * from request Where request_no=' . $request_no . ";");
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data['request_no'] = $row->request_no;
            $data['request_type'] = $row->request_type;
            $data['request_date'] = $row->request_date;
            $data['property_type'] = $row->property_type;
            $data['property_no'] = $row->property_no;
            $data['request_email'] = $row->request_email;
            $data['request_status'] = $row->request_status;
            $data['request_note'] = $row->request_note;
        }
        if (isset($_SESSION['login'])) {
            $this->load->view('admin/header_view');
            $this->load->view('admin/navigation_view');
            $this->load->view('admin/requestdetail_view', $data);
            $this->load->view('admin/footer_view');
        } else {
            $this->index();
        }
    }

}
