<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class general_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function feature_property() {
        $this->login_validate();
        $type = $_POST['type'];
        $property_no_1 = $_POST['property_no_1'];
        $property_no_2 = $_POST['property_no_2'];
        $property_no_3 = $_POST['property_no_3'];

        $data = array(
//            'property_type' => $type,
            'property_no_1' => $property_no_1,
            'property_no_2' => $property_no_2,
            'property_no_3' => $property_no_3
        );
        $this->db->where('property_type',$type);
        $this->db->update('property_feature', $data);
                    redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');

    }

    public function login_validate() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (isset($_SESSION['login'])) {
            
        } else {
            redirect(base_url() . 'index.php/renteasy_admin/index_controller/index');
        }
    }

}
