<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index_controller
 *
 * @author user
 */
class index_controller extends CI_Controller {

    //put your code here
    public function index() {
//                $this->load->view('header_view');
        $data['page'] = 'index';
        $this->load->view('index_view');
        $this->load->view('modal_view', $data);

     
    }

    public function loadLanguage() {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['lang'] = $_GET['lang'];
        $this->config->set_item('language', $_GET['lang']);
        $data['page'] = 'home';
        $this->load->view('header_view');
        $this->load->view('navigation_view', $data);
        $this->load->view('home_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
    }

}
