<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class te_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {


        $this->load->view('test_view');
    }

    public function rent_process() {
        $address = $_POST['address'];
        $year = $_POST['year'];
        $month = $_POST['month'];
        $day = $_POST['day'];
        $bedroom = $_POST['bedroom'];
        $bathroom = $_POST['bathroom'];
        $garage = $_POST['garage'];
        $price = $_POST['price'];
        $suburb = $_POST['suburb'];
        $bus = $_POST['bus'];
        $tram = $_POST['tram'];
        $train = $_POST['train'];
        $others_ch = $_POST['others_ch'];
        $others_en = $_POST['others_en'];
        $test = $address . $year . $month . $day . $bedroom;
        $this->process_image();

        $data['test'] = $test;
        $this->load->view('test_view', $data);
    }

    public function process_image() {
        $dir = 'images/';
        if ($_FILES['uploadfile']['error'] != UPLOAD_ERR_OK) {
            switch ($_FILES['uploadfile']['error']) {
                case UPLOAD_ERR_INI_SIZE: //其值为 1，上传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值
                    die('The upload file exceeds the upload_max_filesize directive in php.ini');
                    break;
                case UPLOAD_ERR_FORM_SIZE: //其值为 2，上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值
                    die('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
                    break;
                case UPLOAD_ERR_PARTIAL: //其值为 3，文件只有部分被上传
                    die('The uploaded file was only partially uploaded.');
                    break;
                case UPLOAD_ERR_NO_FILE: //其值为 4，没有文件被上传
                    die('No file was uploaded.');
                    break;
                case UPLOAD_ERR_NO_TMP_DIR: //其值为 6，找不到临时文件夹
                    die('The server is missing a temporary folder.');
                    break;
                case UPLOAD_ERR_CANT_WRITE: //其值为 7，文件写入失败
                    die('The server failed to write the uploaded file to disk.');
                    break;
                case UPLOAD_ERR_EXTENSION: //其他异常
                    die('File upload stopped by extension.');
                    break;
            }
        }


        list($width, $height, $type, $attr) = getimagesize($_FILES["uploadfile"]['tmp_name']);

        switch ($type) {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($_FILES['uploadfile']['tmp_name']) or die('The file you upload was not supported filetype');
                $ext = '.gif';
                break;
            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($_FILES['uploadfile']['tmp_name']) or die('The file you upload was not supported filetype');
                $ext = '.jpg';
                break;
            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($_FILES['uploadfile']['tmp_name']) or die('The file you upload was not supported filetype');
                $ext = '.png';
                break;
            default:
                die('The file you uploaded was not a supported filetype.');
        }

        $imagename = "rent"."_".$i . $ext;

        switch ($type) {
            case IMAGETYPE_GIF:
                imagegif($image, $dir . '/' . $imagename);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($image, $dir . '/' . $imagename);
                break;
            case IMAGETYPE_PNG:
                imagepng($image, $dir . '/' . $imagename);
                break;
        }
        imagedestroy($image);
    }

}
