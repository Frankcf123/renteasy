<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class search_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index($search = "all") {
        if (!isset($_SESSION)) {
            session_start();
        }

        $search=isset($_POST['searchItem'])?$_POST['searchItem']:'all';
        $this->config->set_item('language', $_SESSION['lang']);
        $this->load->view('header_view');
        $data['page'] = 'home';
        $data['search']=$search;
        if ($search == "all") {
            $query = $this->db->query('SELECT * FROM property_buy where property_buy.status!=\'done\'');
            $data['buy_rows'] = $query->result();

            $query = $this->db->query('SELECT * FROM property_rent where property_rent.status!=\'done\'');
            $data['rent_rows'] = $query->result();

            $query = $this->db->query('SELECT * FROM property_invest where property_invest.status!=\'done\'');
            $data['invest_rows'] = $query->result();
        } else {

        $data['rent_rows'] = $this->rent_search($search);
        $data['buy_rows'] = $this->buy_search($search);
        $data['invest_rows'] = $this->invest_search($search);
        }

        $this->load->view('header_view');
        $this->load->view('navigation_view', $data);
        $this->load->view('search_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
    }

    public function rent_search($search) {
        $this->db->select('*');
        $this->db->from('property_rent');
        $this->db->like('address', $search);
        $this->db->or_like('available_date', $search);
        $this->db->or_like('bedroom', $search);
        $this->db->or_like('bathroom', $search);
        $this->db->or_like('garage', $search);
        $this->db->or_like('price', $search);
        $this->db->or_like('uni_nearby', $search);
        $this->db->or_like('bus_nearby', $search);
        $this->db->or_like('tram_nearby', $search);
        $this->db->or_like('train_station_nearby', $search);
        $this->db->or_like('others_en', $search);
        $this->db->or_like('others_ch', $search);
        $this->db->or_like('suburb', $search);
        $this->db->or_like('facility_nearby_ch', $search);
        $this->db->or_like('facility_nearby_en', $search);
        $this->db->or_like('property_title_en', $search);
        $this->db->or_like('property_title_ch', $search);
        $result = $this->db->get()->result();
        return $result;
    }

    public function buy_search($search) {
        $this->db->select('*');
        $this->db->from('property_buy');
        $this->db->like('address', $search);
        $this->db->or_like('available_date', $search . "");
        $this->db->or_like('bedroom', $search . "");
        $this->db->or_like('bathroom', $search . "");
        $this->db->or_like('garage', $search . "");
        $this->db->or_like('price', $search . "");
        $this->db->or_like('uni_nearby', $search . "");
        $this->db->or_like('bus_nearby', $search . "");
        $this->db->or_like('tram_nearby', $search . "");
        $this->db->or_like('train_station_nearby', $search);
        $this->db->or_like('others_en', $search);
        $this->db->or_like('others_ch', $search);
        $this->db->or_like('suburb', $search);
        $this->db->or_like('facility_nearby_ch', $search);
        $this->db->or_like('facility_nearby_en', $search);
        $this->db->or_like('property_title_en', $search);
        $this->db->or_like('property_title_ch', $search);
        $result = $this->db->get()->result();
        return $result;
    }

    public function invest_search($search) {
        $this->db->select('*');
        $this->db->from('property_invest');
        $this->db->like('property_title_en', $search);
        $this->db->or_like('property_title_ch', $search);
        $this->db->or_like('property_content_en', $search);
        $this->db->or_like('property_content_ch', $search);
        $this->db->or_like('property_tag_en', $search);
        $this->db->or_like('property_tag_ch', $search);
        $this->db->or_like('address', $search);
        $this->db->or_like('available_date', $search);
        $this->db->or_like('price', $search);
        $this->db->or_like('status', $search);
        $this->db->or_like('type', $search);
        $result = $this->db->get()->result();
        return $result;
    }

    public function loadLanguage() {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['lang'] = $_GET['lang'];
        $this->config->set_item('language', $_GET['lang']);
        $data['page'] = 'home';
        $this->load->view('header_view');
        $this->load->view('navigation_view', $data);
        $this->load->view('home_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
    }

}
