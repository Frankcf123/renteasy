<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class general_controller extends CI_Controller {

    public function apply() {



        $property_no = $_POST['property_no'];
        $property_type = $_POST['property_type'];
        $request_email = $_POST['request_email'];
        $request_type = $_POST['request_type'];
        $request_note = $_POST['request_note'];
        $request_date = date("Y-m-d");
        $request_status = 'unresponsive';


        //put into database
        $data = array(
            'property_no' => $property_no,
            'property_type' => $property_type,
            'request_email' => $request_email,
            'request_type' => $request_type,
            'request_note' => $request_note,
            'request_date' => $request_date,
            'request_status' => $request_status
        );
        $this->db->insert('request', $data);
        //send email
        $this->load->model('service_model');
        //useremail,subject,message
        
        $property_details="Property_no:".$property_no."\nProperty_type:".$property_type;
        $request_details="\nRequest_email:".$request_email."\nRequest_type:".$request_type."\nRequest_Note".$request_note.
                "\nRequest_Date".$request_date;
        $this->service_model->sendmail($request_email, $request_type, $request_note,$property_details,$request_details);
    }

}
