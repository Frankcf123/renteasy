<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class buy_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
       public function index($item_page = 1) {
        if (!isset($_SESSION)) {
            session_start();
        }
        //
        $this->config->set_item('language', $_SESSION['lang']);
        $this->load->view('header_view');
        $data['page'] = 'buy';
        $data['count'] = (int)($this->db->count_all('property_buy')/4)+1;
        $data['item_page'] = $item_page;

        //buy property data

        $query = $this->db->query('SELECT * FROM property_buy where property_buy.status!=\'done\'  limit '.(int)($item_page*4-4)." , ".(int)($item_page*4));
        $data['rows'] = $query->result();

        $query = $this->db->query('SELECT * FROM property_feature where property_feature.property_type=\'buy\'');
        $data['feature'] = $query->result();  
        $this->load->view('header_view');
        $this->load->view('navigation_view', $data);
        $this->load->view('buy_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
           $query = $this->db->query('SELECT DISTINCT suburb FROM property_buy');
        $re = $query->result();
       $_SESSION['suburb']=$re;
    }

    public function loadLanguage() {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['lang'] = $_GET['lang'];
        $this->config->set_item('language', $_SESSION['lang']);
        $this->load->view('header_view');
        $data['page'] = 'buy';
        
        $query = $this->db->query('SELECT * FROM property_feature where property_feature.property_type=\'buy\'');
        $data['feature'] = $query->result();  
        //buy property data
        $this->db->select('*');
        $this->db->from('property_buy');
        $query = $this->db->query('SELECT * FROM property_buy');
        $data['rows'] = $query->result();

        $this->load->view('header_view');
        $this->load->view('navigation_view', $data);
        $this->load->view('buy_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
           $query = $this->db->query('SELECT DISTINCT suburb FROM property_buy');
        $re = $query->result();
       $_SESSION['suburb']=$re;
    }
}
