<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class privacy_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
        public function index() {
        if (!isset($_SESSION)) {
            session_start();
        }
        
        $this->config->set_item('language', $_SESSION['lang']);
        $data['page']='home';
            //buy property data
   


        $this->load->view('header_view');
        $this->load->view('navigation_view',$data);
        $this->load->view('privacy_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
    }

    public function loadLanguage() {
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['lang'] = $_GET['lang'];
        $this->config->set_item('language', $_GET['lang']);
        $data['page'] = 'home';
        $this->load->view('header_view');
        $this->load->view('navigation_view', $data);
        $this->load->view('home_view');
        $this->load->view('modal_view');
        $this->load->view('footer_view');
    }


}