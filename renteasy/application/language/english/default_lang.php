<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//navi
$lang['title'] = 'RentEasy';
$lang['search_prompt']='Enter keywords';
$lang['home'] = 'HOME';
$lang['rent'] = 'RENT';
$lang['buy'] = 'BUY';
$lang['invest'] = 'INVEST';
$lang['discussion'] = 'DISCUSSION';

//footer
$lang['copyright_1']='Copyright ©';
$lang['copyright_2']='EasyRent 2015';
$lang['privacy_policy']='Privacy Policy';


//home
$lang['quick_find']='QUICK FIND';
$lang['home_title_1']='Find melbourne properties to';
//$lang['home_title_2']='TO RENT,BUY,INVEST';


//rent
$lang['refine_search'] = 'Refine Your Search';
$lang['region'] = 'REGION';
$lang['price'] = 'PRICE';
$lang['bedroom'] = 'BEDROOMS';
$lang['garbage'] = 'GARBAGES';
$lang['search'] = 'SEARCH';
$lang['sort_by']='Sort By';
$lang['price_low_to_high']='Price(low to high)';
$lang['price_high_to_low']='Price(high to low)';
$lang['learn_more']='Learn More';
$lang['load_more']='Load More';

$lang['bathroom']='Bathrooms';
$lang['facility']='Facility nearby';
$lang['transportation']='Transportation';
$lang['others']='Others';
$lang['address']='Address';
$lang['uni']='Uni nearby';


//modal
$lang['select_option']='Select Your option';


//other
$lang['language']='english';
$lang['search_small']='search';
$lang['apply_now']='Apply Now';
//search 
$lang['search_result']='Search Result';
$lang['show_result']='Show Result';
$lang['all_small']='All';
$lang['rent_small']='Rent';
$lang['buy_small']='Buy';
$lang['invest_small']='Invest';


//discussion
$lang['discussion_forum']='Discussion Form';
$lang['create_post']='Create A Post';
$lang['category']='Category';

//item
$lang['location_info']='Location info';
$lang['basic_info']='Basic info';
$lang['other_info']='Others';
$lang['property_detail']='Property Details';
$lang['available_date']='Avaiable After';
$lang['apply_info']='please leave your detailes we will arrange inspection for you';
$lang['phone_num']='Phone Num';
$lang['email']='Email';
$lang['Note']='Note';
$lang['garage']='Garages';

$lang['week']='Week';

$lang['apply']='Apply/Ask a Question';
//search
$lang['all']='All';

//invest
$lang['type']='Type';
$lang['description']='Description';
$lang['tags']='Tags';

//request
$lang['email']='Email';
$lang['note']='Note';
$lang['request_type']='Request Type';
$lang['request_normal']="Normal Question";
$lang['request_inspection']="Request Inspection:";
$lang['mailsent_title']="Request Received.";
$lang['mailsent_propmpt']="You request has been sent successfully and we will contact you within 24 hrs.";

//blog
$lang['rent_tag']='RENT';
$lang['buy_tag']='BUY';
$lang['invest_tag']='INVEST';
$lang['others_tag']='OTHERS';
$lang['blog_detail']='Blog Detail';


$lang['all']='All';
$lang['melbourne']='Melbourne';
$lang['privacy_detail']="Privacy Rule";
$lang['privacy_content']="privacy content";
$lang['about_renteasy']="About Renteasy";
$lang['about_content']="about_content";