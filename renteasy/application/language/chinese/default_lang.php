<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//navi
$lang['title'] = '易租网RentEasy';
$lang['search_prompt']='输入关键词';
$lang['home'] = '主页';
$lang['rent'] = '租房';
$lang['buy'] = '买房';
$lang['invest'] = '投资';
$lang['discussion'] = '讨论区';

//footer
$lang['copyright_1']='Copyright ©';
$lang['copyright_2']='易租网 2015';

$lang['privacy_policy']='隐私条约';


//home
$lang['quick_find']='快速选房';
$lang['home_title_1']='找寻墨尔本的房子';
//$lang['home_title_2']='租房，买房，投资';


//rent
$lang['refine_search'] = '精确查询';
$lang['region'] = '地区';
$lang['price'] = '价格';
$lang['bedroom'] = '房间数';
$lang['garbage'] = '车库数';
$lang['search'] = '搜素';
$lang['sort_by']='排序';
$lang['price_low_to_high']='价格（低到高）';
$lang['price_high_to_low']='价格（低到高）';
$lang['learn_more']='了解更多';
$lang['load_more']='加载更多';

$lang['bathroom']='卫生间';
$lang['facility']='周边设施';
$lang['transportation']='附近交通';
$lang['others']='其他';
$lang['address']='地址';
$lang['uni']='周边学校';


//modal
$lang['select_option']='选择一个选项';
$lang['search_small']='搜索';
$lang['appy_now']='立刻申请';


//other
$lang['language']='chinese';
$lang['search_small']='搜索';
$lang['apply_now']='立刻申请';
//search 
$lang['search_result']='搜素结果';
$lang['show_result']='显示结果';
$lang['all_small']='全部';
$lang['rent_small']='租房';
$lang['buy_small']='买房';
$lang['invest_small']='投资';

//discussion
$lang['discussion_forum']='论坛';
$lang['create_post']='发表文章';
$lang['category']='分类';

//item
$lang['location_info']='地址信息';
$lang['basic_info']='基本信息';
$lang['other_info']='其他';
$lang['property_detail']='房产信息';
$lang['available_date']='可入住日期';
$lang['apply_info']='请留下联系方式我们会在24小时内联系您';
$lang['phone_num']='电话号码';
$lang['email']='邮箱';
$lang['note']='备注';
$lang['garage']='车库';
$lang['apply']='申请/提问';
$lang['week']='周';

//search
$lang['all']='全部';

//invest
$lang['type']='类型';
$lang['description']='描述';
$lang['tags']='标签';

//request
$lang['email']='邮箱';
$lang['note']='备注';
$lang['request_type']='请求类型';
$lang['request_normal']="普通问题";
$lang['request_inspection']="申请看房";
$lang['mailsent_title']="请求成功接收";
$lang['mailsent_propmpt']="您的请求已经成功接收，我们将在24个小时之内联系您。";

//blog
$lang['rent_tag']='租房';
$lang['buy_tag']='买房';
$lang['invest_tag']='投资';
$lang['others_tag']='其他';
$lang['blog_detail']='博客信息';
//constants
$lang['all']='全部';
$lang['melbourne']='墨尔本';
$lang['privacy_detail']="隐私信息";
$lang['privacy_content']="内容";
$lang['about_renteasy']="关于易租网";
$lang['about_content']="about_content";