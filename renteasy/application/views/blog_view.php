<br/><br/><br/>

<br/><br/>
---------------------------------------------


<!-- Page Heading/Breadcrumbs -->

<div class="container col-lg-12">







    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="color:#80720B"><?php echo lang('blog_detail'); ?>

                </h1>
                <ol class="breadcrumb">
                    <li>   <a href='<?php echo base_url() ?>index.php/home_view/index' > <?php echo lang('home'); ?></a>
                    </li>
                    <li class="active"><?php echo lang('property_detail'); ?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <br/><br/>

        <?php
        if (!isset($_SESSION)) {
            session_start();
        }
        //
        $lan = $_SESSION['lang'];
        $row = $rows[0];
        ?>

        <div class="row container">
            <div class="jumbotron" style="">
                <h1 style="text-align: center">
                    <?php
                    if ($lan == 'english') {
                        echo $row->blog_title_en;
                    } else {
                        echo $row->blog_title_ch;
                    }
                    ?>

                    <br/></h1>
                <p style="color:goldenrod"><?php echo lang('category') ?>:&nbsp;




                    <span class="btn btn-info"><?php
                        echo lang('' . $row->blog_tag . '');
                        ?></span></p>
                <p><br/>

                    <?php
                    if ($lan == 'english') {
                        echo $row->blog_content_en;
                    } else {
                        echo $row->blog_content_ch;
                    }
                    ?>
                </p>
            </div>
        </div>
        <br/><br/>
        <div class="col-md-12">



        </div>
    </div>



</div>





