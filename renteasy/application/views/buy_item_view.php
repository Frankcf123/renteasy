<br/><br/><br/>

<br/><br/>
---------------------------------------------


<!-- Page Heading/Breadcrumbs -->

<div class="container col-lg-12">







    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="color:#80720B"><?php echo lang('property_detail'); ?>

                </h1>
                <ol class="breadcrumb">
                    <li>   <a href='<?php echo base_url() ?>index.php/home_view/index' > <?php echo lang('home'); ?></a>
                    </li>
                    <li class="active"><?php echo lang('property_detail'); ?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <br/><br/>


        <div class="row">
            <?php foreach ($rows as $row) { ?>

                <div class="col-md-6">
<br/>
                    <div id="item_carousel" class="carousel slide" data-interval="4000" data-ride="carousel">
                        <!-- 轮播（Carousel）指标 -->
                        <ol class="carousel-indicators">
                            <li data-target="#item_carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#item_carousel" data-slide-to="1"></li>
                            <li data-target="#item_carousel" data-slide-to="2"></li>
                            <li data-target="#item_carousel" data-slide-to="3"></li>
                            <li data-target="#item_carousel" data-slide-to="4"></li>

                        </ol>   
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="img-responsive"
                                     src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $row->latitude . "," . $row->longitude; ?>&zoom=11&size=600x450&sensor=false"
                                     style="width:600px;height:450px;">

                            </div>
                            <?php for ($i = 1; $i < 5; $i++) { ?>
                                <div class="item">
                                    <img class="img-responsive"
                                        src="<?php echo base_url(); ?>/upload_files/buy_images/buy_<?php echo $row->property_no; ?>_<?php echo $i; ?>.jpg" 
                                        style="width:600px;height:450px;">

                                </div>

                            <?php } ?>
                        </div>
                        <a class="left carousel-control" href="#item_carousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a class="right carousel-control" href="#item_carousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>

                    </div> 

                </div>


                <div class="col-md-6" style="color:goldenrod">

                    <h3 style="color:greenyellow">  <?php
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        //
                        $lan = $_SESSION['lang'];

                        if ($lan == 'english') {
                            echo $row->property_title_en;
                        } else {
                            echo $row->property_title_ch;
                        }
                        ?></h3>
                    <h2 class="pull-right" style="color:blue;background: red;padding: 3px;">RENT</h2>
                    <hr>
                    <h3><?php echo lang('basic_info'); ?>::</h3>
                    <ul>

                        <li ><?php echo lang('price'); ?>(/week):
                            <span style="color:gold">$<?php echo $row->price; ?></span></li>
                        <li><?php echo lang('bedroom'); ?>:
                            <span style="color:gold"><?php echo $row->bedroom; ?></span></li>
                        <li><?php echo lang('bathroom'); ?>:
                            <span style="color:gold"><?php echo $row->bathroom; ?></span></li>
                        <li><?php echo lang('garbage'); ?>:
                            <span style="color:gold"><?php echo $row->garage; ?></span></li>
                        <li><?php echo lang('available_date'); ?>:
                            <span style="color:gold"><?php echo $row->available_date; ?></span></li>

                    </ul>

                    <h3><?php echo lang('location_info'); ?>::</h3>
                    <ul>
                        <li><?php echo lang('address'); ?>:         
                                <span style="color:gold"><?php echo $row->address; ?></span>
                            <a class="" href="http://www.google.cn/maps/@<?php echo $row->latitude . "," . $row->longitude ?>,16z">
                                <img src="<?php echo base_url();?>/assets/img/googlemap_logo.jpg" width="30" height="30"/></a>                       
                        </li>
                        <li><?php echo lang('facility'); ?>:
                            <span style="color:gold">:
                                <?php
                                if (!isset($_SESSION)) {
                                    session_start();
                                }
                                if ($_SESSION['lang'] == 'english') {
                                    echo $row->facility_nearby_en;
                                } else {
                                    echo $row->facility_nearby_ch;
                                }
                                ?></li>
                        <li><?php echo lang('transportation'); ?>:
                            <ul>
                                <li><span style="color:gold">bus:<?php echo $row->bus_nearby; ?></span></li>
                                <li><span style="color:gold">tram: <?php echo $row->tram_nearby; ?> </span></li>
                                <li><span style="color:gold">train station: <?php echo $row->train_station_nearby; ?></span></li>

                            </ul>
                        </li>
                        <li><?php echo lang('uni'); ?>:
                            <span style="color:gold"><?php echo $row->uni_nearby; ?></span>
                        </li>
                    </ul>
                    <h3><?php echo lang('other_info'); ?>:</h3>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span style="color:gold"> 
                            <?php
                            if (!isset($_SESSION)) {
                                session_start();
                            }
                            if ($_SESSION['lang'] == 'english') {
                                echo $row->others_en;
                            } else {
                                echo $row->others_ch;
                            }
                            ?>
                        </span></p>
                    <hr>

                    <a href="#applyModal" 
                       data-toggle="modal" 
                       data-target="#applyModal" style="color:black" class="pull-right btn btn-primary btn-block btn-lg">
                        <?php echo lang('apply'); ?></a>
                </div>



            <?php } ?>
        </div>
        <br/><br/>
        <div class="col-md-12">



        </div>
    </div>



</div>


<!--language modal-->
<div class="modal" id="applyModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: black">
                <!--                <button type="button" class="close" 
                                        data-dismiss="modal" aria-hidden="true">
                                    <h2 style="color:goldenrod">
                                        &times;</h2>
                                </button>-->
                <h4 class="modal-title" id="myModalLabel" style="color:gold">
                    <?php echo lang('apply_info'); ?>:
                </h4>

            </div>
            <div class="modal-body" style="background-color: black">
                <form class="form-horizontal" role="form"  method="post"
                      action="<?php echo base_url(); ?>index.php/general_controller/apply">
                    <input type="hidden" name="property_no" value="81">
                    <input type="hidden" name="property_type" value="buy">
                    <div class="form-group">
                        <div class="col-sm-2"><label style="color:goldenrod" for="email" class="control-label">
                                <?php echo lang('email') ?></label></div>
                        <div class="col-sm-10">
                            <input type="text" name="request_email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"><label style="color:goldenrod" for="email" class="control-label">
                                <?php echo lang('request_type') ?></label></div>
                        <div class="col-sm-10 form-control">
                            <select name="request_type">
                                <option value="question"><?php echo lang('request_normal'); ?></option>
                                <option value="inspecton"><?php echo lang('request_inspection'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"><label style="color:goldenrod" for="email" class="control-label">
                                <?php echo lang('note') ?></label></div>
                        <div class="col-sm-10">
                            <textarea name="request_note" class="form-control"> </textarea>
                        </div>
                    </div>

                    <div class="form-group"><div class="col-sm-offset-7 col-sm-10">
                            <button type="submit" class="btn btn-primary"><span style="color:black"><?php echo lang('apply'); ?></span></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>









