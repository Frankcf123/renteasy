<br/><br/><br/>

<br/><br/>
---------------------------------------------


<!-- Page Heading/Breadcrumbs -->

<div class="container col-lg-12">

    <div class="col-md-12">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row" >
            <div class="col-lg-12">
                <h1 class="page-header" style="color:#80720B"><?php echo lang('discussion_forum'); ?>
<!--                    <span class="pull-right"><button class="btn btn-primary btn-lg"><?php echo lang('create_post');?></button></span>-->
                </h1>
                
                <ol class="breadcrumb" style="background-color: gainsboro">
                    <li>   <a href='<?php echo base_url(); ?>index.php/home_controller/index' > 
                        <?php echo lang('home');?></a>
                    </li>
                    <li class="active"><?php echo lang('discussion_forum'); ?></li>
                </ol>
            </div>
        </div>
        <!-- /.row --> 
        <br/><br/>     


    </div>


    <!--the details-->
    <div class="container col-md-12 col-lg-12 col-sm-12" style="align:center">
        <ul class="media-list">
           <?php
                if (!isset($_SESSION)) {
                    session_start();
                }
                //
                $lan = $_SESSION['lang'];
                ?>
                <?php foreach($rows as $row){?>
                    <li class="media col-md-12 col-lg-12 col-sm-12"  style="background: ;text-align: center;">
                    <a class="pull-left" href="#">
                        <img class="media-object img-responsive img-centered img-circle" src="http://placehold.it/64x64"></a>
                    <div class="media-body" style="padding-left: 20px">
                        <br/>
                        <a href="<?php echo base_url();?>index.php/blog_controller/index/<?php echo $row->blog_no;?>">
                            <h4 class="media-heading"  style="color:#D9D11C">
                                <span style="color:skyblue;padding-right: 20px">
                                    [<?php 
                                        echo lang(''.$row->blog_tag.'');
                                    ?>]
                                </span><?php 
                                if($lan=='english'){
                                    echo $row->blog_title_en;
                                }else{
                                    echo $row->blog_title_ch;
                                }
                                ?></h4></a>
                    </div>

                    <hr>

                </li>
                <?php }?>
        </ul>
    </div>



  <!--load more-->
        <br/><br/><br/>
        <nav>
            <ul class="pagination">
                <li><a href="<?php
                    if ($item_page == 1 || $count == 1) {
                        echo "#";
                    } else {
                        echo base_url() . 'index.php/rent_controller/index' . (int) ((int) $item_page - 1);
                    }
                    ?>">

                        <span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
                <?php
                for ($i = 1; $i < $count + 1; $i++) {
                    if ($i == $item_page) {
                        echo "<li class=\"active\">";
                    } else {
                        echo "<li>";
                    }

                    echo "<a href = \"" . base_url() . "index.php/rent_controller/index/" . $item_page . "\" >$count</a></li>";
                }
                ?>



                <li><a href="<?php
                    if ($item_page == $count) {
                        echo "#";
                    } else {
                        echo base_url() . 'index.php/rent_controller/index' . (int) ((int) $item_page + 1);
                    }
                    ?>">
                        <span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
            </ul>
        </nav>

        <br/>
        <br/><br/><br/>


</div>