<html>
    <head>
        <title></title>
        <style type="text/css"></style>
    </head>
    <body>
        <form 
            action="<?php echo base_url(); ?>index.php/renteasy_admin/rent_controller/test"
            method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td><?php echo $vi;?></td>
                    <td><input type="text" name="username" /></td>
                </tr>
                <tr>
                    <td>Upload image*</td>
                    <td><input type="file" name="uploadfile"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <small><em> * Acceptable image formats include: GIF, JPG/JPEG and PNG.</em></small>
                    </td>
                </tr>
                <tr>
                    <td>Image Caption</td>
                    <td><input type="text" name="caption"/></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <input type="submit" name="submit" value="Upload" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>