<br/><br/><br/>

<br/><br/>
---------------------------------------------


<!-- Page Heading/Breadcrumbs -->

<div class="container col-lg-12">



    <!--page divider-->
    <div class="col-md-12">

        <div id="myCarousel" class="carousel slide" data-interval="2500" class="carousel slide " data-ride="carousel">
            <!-- 轮播（Carousel）指标 -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>   
             <div class="carousel-inner">
                <?php
                if (!isset($_SESSION)) {
                    session_start();
                }
                $lan = $_SESSION['lang'];
                ?>
                
                <div class="item active">
                    <img 
                        src="<?php echo base_url(); ?>/upload_files/buy_images/buy_<?php echo $feature[0]->property_no_1 . "_", $feature[0]->property_no_1;?>.jpg" 
                        style="width:1200px;height:300px;">
                    <div class="carousel-caption">
                        <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $feature[0]->property_no_1; ?>"><h2>
                                <?php
                                foreach ($rows as $ra) {
                                    if ($ra->property_no == $feature[0]->property_no_1) {
                                        if ($lan == 'english') {
                                            echo $ra->property_title_en;
                                        } else {
                                            echo $ra->property_title_ch;
                                        }
                                    }
                                }
                                ?>

                            </h2><p></p></a>
                    </div>
                </div>
                   <div class="item">
                    <img 
                        src="<?php echo base_url(); ?>/upload_files/buy_images/buy_<?php echo $feature[0]->property_no_2 . "_", $feature[0]->property_no_2; ?>.jpg" 
                        style="width:1200px;height:300px;">
                    <div class="carousel-caption">
                        <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $feature[0]->property_no_2; ?>"><h2>
                                <?php
                                foreach ($rows as $ra) {
                                    if ($ra->property_no == $feature[0]->property_no_2) {
                                        if ($lan == 'english') {
                                            echo $ra->property_title_en;
                                        } else {
                                            echo $ra->property_title_ch;
                                        }
                                    }
                                }
                                ?>

                            </h2><p></p></a>
                    </div>
                </div>
                   <div class="item">
                    <img 
                        src="<?php echo base_url(); ?>/upload_files/buy_images/buy_<?php echo $feature[0]->property_no_3 . "_", $feature[0]->property_no_3;?>.jpg" 
                        style="width:1200px;height:300px;">
                    <div class="carousel-caption">
                        <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $feature[0]->property_no_3; ?>"><h2>
                                <?php
                                foreach ($rows as $ra) {
                                    if ($ra->property_no == $feature[0]->property_no_3) {
                                        if ($lan == 'english') {
                                            echo $ra->property_title_en;
                                        } else {
                                            echo $ra->property_title_ch;
                                        }
                                    }
                                }
                                ?>

                            </h2><p></p></a>
                    </div>
                </div>
            </div>

        </div> 

        <h1 class="page-header" style="color:#80720B">
            <?php echo lang('buy'); ?>
            <small > -------<a href="<?php echo base_url() . 'index.php/home_controller' ?>" style="color:grey"><?php echo lang('home') ?></a>
                / <a href="<?php echo base_url() . 'index.php/buy_controller' ?>" style="color:grey"><?php echo lang('buy') ?></a>
                -------</small>
        </h1>



    </div>


    <div class="col-md-12">
        <!--                <h1 class="page-header" style="color:yellow">Contact us
        
                        </h1>-->

        <ol class="breadcrumb" style="background:#BFAF9D">
    <li><?php echo lang('region'); ?>:
                    <select name="suburb">
                        <option value="all"><?php echo lang('all');?></option>
                        <?php
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        foreach ($_SESSION['suburb'] as $ar) {
                           
                        ?>
                        <option value="<?php echo $ar->suburb;?>"> <?php echo $ar->suburb;?></option>
                        <?php }?>
                       
                    </select></li>
         
                <?php echo lang('price'); ?>($):
                <select>
                    <?php
                    for ($i = 0; $i < 11; $i++) {
                        ?>
                        <option value="<?php echo $i * 100; ?>"><?php echo $i * 100; ?></option>
                    <?php } ?>
                </select>
                --
                <select>
                    <?php
                    for ($i = 1; $i < 11; $i++) {
                        ?>
                        <option value="<?php echo $i * 100; ?>"><?php echo $i * 100; ?></option>
                    <?php } ?>
                </select></li>
            <li><?php echo lang('bedroom'); ?>:
                <select>
                    <?php
                    for ($i = 1; $i < 6; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select></li>
            <li><?php echo lang('garbage'); ?>:
                <select>
                    <?php
                    for ($i = 0; $i < 3; $i++) {
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select></li>
            <li>
                <button class="btn btn-primary">
                    <span style="color:black"><?php echo lang('search'); ?></span></button>
            </li>


        </ol>
    </div>
    <!--the details-->
    <div class="container col-md-12 col-lg-12 col-sm-12" style="align:center">
        <?php foreach ($rows as $row) { ?>
            <div class="panel panel-primary">
                <?php
                if (!isset($_SESSION)) {
                    session_start();
                }
                //
                $lan = $_SESSION['lang'];
                ?>
                <div class="panel-heading pull" style="background: #F2F2EB">
                    <span class="pull-right">
                    </span>
                </div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <div id="slide_buy_<?php echo $row->property_no; ?>" data-interval="false" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                        <img 
                                            src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $row->latitude.",".$row->longitude;?>&zoom=11&size=400x263&sensor=false"
                                            class="img-thumbnail img-responsive img-centered" width="400" height="280"
                                            ></a>
                                </div>
                                <?php for ($i = 1; $i < 5; $i++) { ?>
                                    <div class="item ">
                                        <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                            <img 
                                                src="<?php echo base_url(); ?>/upload_files/buy_images/buy_<?php echo $row->property_no; ?>_<?php echo $i; ?>.jpg" 
                                              class="img-thumbnail img-responsive img-centered"  width="400" height="280"></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <a class="left carousel-control" href="#slide_buy_<?php echo $row->property_no; ?>" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span></a>
                            <a class="right carousel-control" href="#slide_buy_<?php echo $row->property_no; ?>" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h4 style="color:goldenrod;text-align: center">
                            <a style="color:goldenrod;" href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                <?php
                                if ($lan == 'english') {
                                    echo $row->property_title_en;
                                } else {
                                    echo $row->property_title_ch;
                                }
                                ?></a></h4>
                        <p style="color:goldenrod;text-align: center">  
                        <ul class="list-inline mrg-0 btm-mrg-10 clr-535353" style="text-align: center" >
                            <li><strong>$<?php echo $row->price; ?></strong></li>
                            <li style="list-style: none">|</li>
                            <li><strong><?php echo $row->bedroom; ?></strong> <?php echo lang('bedroom'); ?> </li>
                            <li style="list-style: none">|</li>
                            <li><strong><?php echo $row->garage; ?></strong> <?php echo lang('garage'); ?> </li>
                            <li style="list-style: none">|</li>
                            <li><strong><?php echo $row->bathroom; ?></strong> <?php echo lang('bathroom'); ?> </li>
                            <li style="list-style: none">|</li>

                        </ul>
                        <br/>

                        <h4 style="color:black;text-align: center" class="pull-left">
                                <?php echo lang('address'); ?>:
                            <a  style="color:gray" href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
    <?php echo $row->address; ?></a></h4>
                        <br/>
                        </p>
                        <hr>

                        <a class="btn btn-primary btn-lg btn-block" 
                           href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
    <?php echo lang('learn_more'); ?></a>
                    </div>
                </div>
            </div> 
<?php } ?>

        <!--load more-->
        <br/><br/><br/>
        <nav>
            <ul class="pagination">
                <li><a href="<?php
                    if ($item_page == 1 || $count == 1) {
                        echo "#";
                    } else {
                        echo base_url() . 'index.php/buy_controller/index' . (int) ((int) $item_page - 1);
                    }
                    ?>">

                        <span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
                <?php
                for ($i = 1; $i < $count + 1; $i++) {
                    if ($i == $item_page) {
                        echo "<li class=\"active\">";
                    } else {
                        echo "<li>";
                    }

                    echo "<a href = \"" . base_url() . "index.php/buy_controller/index/" . $item_page . "\" >$count</a></li>";
                }
                ?>



                <li><a href="<?php
                    if ($item_page == $count) {
                        echo "#";
                    } else {
                        echo base_url() . 'index.php/buy_controller/index' . (int) ((int) $item_page + 1);
                    }
                    ?>">
                        <span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
            </ul>
        </nav>

        <br/>
        <br/><br/><br/>


    </div>

</div>




