<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>RentEasy</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>/assets/css/agency.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/assets/css/add.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>/assets/font-awesome-4.1.0/css/font-awesome.css" rel="stylesheet" type="text/css">

        <link href="<?php echo base_url(); ?>/assets/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
                <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            /* Custom Styles */
            ul.nav-tabs{
                width: 200px;
                margin-top: 20px;
                border-radius: 4px;
                border: 1px solid #ddd;
                box-shadow: 0 1px 4px rgba(0, 0, 0, 0.067);
            }
            ul.nav-tabs li{
                margin: 0;
                border-top: 1px solid #ddd;
            }
            ul.nav-tabs li:first-child{
                border-top: none;
            }
            ul.nav-tabs li a{
                margin: 0;
                padding: 8px 16px;
                border-radius: 0;
            }
            ul.nav-tabs li.active a, ul.nav-tabs li.active a:hover{
                color: #fff;
                background: #0088cc;
                border: 1px solid #0088cc;
            }
            ul.nav-tabs li:first-child a{
                border-radius: 4px 4px 0 0;
            }
            ul.nav-tabs li:last-child a{
                border-radius: 0 0 4px 4px;
            }
            ul.nav-tabs.affix{
                top: 30px; /* Set the top position of pinned element */
            }
            .carousel
        </style>

        
    </head>
    <?php $this->lang->load('default');
    ?>