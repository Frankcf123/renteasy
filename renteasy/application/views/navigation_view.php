<body id="page-top" class="index" >

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" style="padding-left: 5px">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <strong>
                    <a class="navbar-brand page-scroll" href="<?php echo base_url(); ?>index.php/home_controller">
                        <?php echo lang('title'); ?>
                    </a></strong>
                <a href="#languageModal" 
                   data-toggle="modal" 
                   data-target="#languageModal">
                    <img src="<?php echo base_url(); ?>/assets/img/language/<?php echo lang('language'); ?>_flag_small.png"
                         class="img-thumbnail"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">

                    <li style="padding-top: 9px;padding-right: 15px">

                        <div class="input-append">
                            <form id="custom-search-form" class="form-search form-horizontal pull-right"
                                  method="post" action="<?php echo base_url(); ?>index.php/search_controller" >
                                
                                <input class="span2" id="searchItem" name="searchItem"
                                       type="search" style="color:yellowgreen" 
                                       placeholder="  <?php echo lang('search_prompt'); ?>">
                                <button class="btn-primary" type="submit" role="submit">
                                    <span class="glyphicon glyphicon-search"></span></button>  
                            </form>
                        </div>
                
<br/>
                    </li>
                    
                    <li>
                        <a class="page-scroll" href="<?php echo base_url(); ?>index.php/home_controller ">
                            <span style="color:
                                  <?php if($page=='home') echo 'gold'; ?>" >
                            <?php echo lang('home'); ?></span></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url(); ?>index.php/rent_controller">  
                            <span style="color:
                                  <?php if($page=='rent') echo 'gold'; ?>" >
                           <?php echo lang('rent'); ?></span></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url(); ?>index.php/buy_controller">
                            <span style="color:
                                  <?php if($page=='buy') echo 'gold'; ?>" >
                             <?php echo lang('buy'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url(); ?>index.php/invest_controller"> 
                            <span style="color:
                                  <?php if($page=='invest') echo 'gold'; ?>" >
                           <?php echo lang('invest'); ?></span></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo base_url(); ?>index.php/discussion_controller">
                             <span style="color:
                                  <?php if($page=='discussion') echo 'gold'; ?>" >
                          <?php echo lang('discussion'); ?></span></a>
                    </li>
<!--                    <li >
                        <a class="page-scroll" href="<?php echo base_url(); ?>index.php/quickpick_controller">
                            <button class="btn-primary">
                                <div style="color:buttontext">
                                    <?php echo lang('quick_find'); ?>
                                </div></button></a>
                    </li>-->


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
