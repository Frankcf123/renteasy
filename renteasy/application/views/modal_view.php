
<!--language modal-->
<div class="modal" id="languageModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: black">
                <!--                <button type="button" class="close" 
                                        data-dismiss="modal" aria-hidden="true">
                                    <h2 style="color:goldenrod">
                                        &times;</h2>
                                </button>-->
                <h4 class="modal-title" id="myModalLabel" style="color:gold">
                    Language/语言
                </h4>

            </div>
            <div class="modal-body" style="background-color: black">
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <a href="<?php echo base_url(); ?>index.php/<?php echo $page; ?>_controller/loadLanguage?lang=chinese">
                        <h1>中文</h1>
                        <img class="img-responsive " src="<?php echo base_url(); ?>/assets/img/language/chinese_flag.png"></a>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6"> 
                    <a href="<?php echo base_url(); ?>index.php/<?php echo $page ?>_controller/loadLanguage?lang=english">
                        <h1>English</h1>
                        <img class="img-responsive " src="<?php echo base_url(); ?>/assets/img/language/english_flag.png"></a>
                </div>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>



<!--language modal-->
<div class="modal" id="loginModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: black">
                <!--                <button type="button" class="close" 
                                        data-dismiss="modal" aria-hidden="true">
                                    <h2 style="color:goldenrod">
                                        &times;</h2>
                                </button>-->
                <h4 class="modal-title" id="myModalLabel" style="color:gold">
                    Admin Login
                </h4>

            </div>
            <div class="modal-body" style="background-color: black; color:goldenrod">
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>index.php/renteasy_admin/index_controller/login">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="username" class="control-label">UserName:</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="username" name="username" class="form-control" id="inputEmail3" placeholder="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="inputPassword" class="control-label">Password</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Sign in</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>


