
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span style='color:goldenrod'><strong>Dashboard</strong></span> 
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <br/>        <br/>
        <br/>

        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#all" data-toggle="tab">
                                All Requests:(<?php echo $count; ?>)</a>
                        </li>
                        <li><a href="#unsolved" data-toggle="tab">Unsolved Requests
                                <span style="color:red">
                                    <?php
                                    $number = 0;
                                    foreach ($requests as $row) {
                                        if ($row->request_status == "unresponsive") {
                                            $number++;
                                        }
                                    }
                                    if ($number > 0) {
                                        echo "(" . $number . ")";
                                    }
                                    ?></span></a></li>

                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="all">
                            <table class="container table table-striped">
                                <thead>
                                <th>Request No:</th>
                                <th>Request Type:</th>
                                <th>Requested Date:</th>
                                <th>Status</th>
                                <th></th>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($requests as $row) {
                                        echo "<tr>";
                                        echo "<td>" . $row->request_no . "</td>";
                                        echo "<td>" . $row->request_type . "</td>";
                                        echo "<td>" . $row->request_date . "</td>";
                                        echo "<td>" . $row->request_status . "</td>";
                                        ?>

                                    <td> <a href="<?php echo base_url() . 'index.php/renteasy_admin/index_controller/request_details/' . $row->request_no; ?>"
                                            class="btn btn-primary">View Details</a></td>
                                        <?php
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>                       
                        </div>
                        <div class="tab-pane fade" id="unsolved">
                            <table class="container table table-striped">
                                <thead>
                                <th>Request No:</th>
                                <th>Request Type:</th>
                                <th>Requested Date:</th>
                                <th>Status</th>
                                <th></th>
                                <th></th>

                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($requests as $row) {
                                        if ($row->request_status == "unresponsive") {
                                            echo "<tr>";
                                            echo "<td>" . $row->request_no . "</td>";
                                            echo "<td>" . $row->request_type . "</td>";
                                            echo "<td>" . $row->request_date . "</td>";
                                            echo "<td>" . $row->request_status . "</td>";
                                            ?>

                                        <td> <a href="<?php echo base_url() . 'index.php/renteasy_admin/index_controller/request_details/' . $row->request_no; ?>"
                                                class="btn btn-primary">View Details</a></td>
                                            <?php
                                            echo "<td><a href=\"" . base_url() . "index.php/renteasy_admin/index_controller/done/" . $row->request_no . "\">"
                                            . "<button class=\"btn btn-danger\">Mark as Done</button></a></td>";
                                            echo "</tr>";
                                        }
                                    }
                                    ?>


                                </tbody>
                            </table>                       
                        </div>

                    </div>
                    <script>
                        $(function() {
                            $('#myTab li:eq(1) a').tab('show');
                        });
                    </script>


                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->








</div>
<!-- /#wrapper -->

