
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span style='color:forestgreen'>INVEST</span> <small>add,delete,update</small>
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-lg-4 col-md-12" >
                <a data-toggle="modal" 
                   data-target="#addModal" >
                    <button class="btn btn-primary btn-info " >Add Property</button>
                </a>
            </div>
         
            <div class="col-lg-4 col-md-12"  >
                <a data-toggle="modal" 
                   data-target="#changeModal" >
                    <button class="btn btn-primary btn-info  " >Change Property Status</button>
                </a>
            </div>      
            <div class="col-lg-4 col-md-12"  >
                <a data-toggle="modal" 
                   data-target="#featureModal" >
                    <button class="btn btn-primary btn-info" >Change Featured property</button>
                </a>
            </div>              
        </div>
        <br/>        <br/>
        <br/>

        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-bar-chart-o fa-fw"></i>All Invest Properties(<?php echo $count; ?>) </h3>
                    </div>
                    <div class="panel-body">
                        <!--<div id="morris-area-chart"></div>-->
                        <table class="container table table-striped">
                            <thead>
                            <th>Property_No</th>
                            <th>title</th>
                            <th>Price($:pw):</th>
                            <th>Available Date:</th>
                            <th>Status</th>
                            <th></th>
                            <th></th> 
                            </thead>
                            <tbody>
                                <?php
                                foreach ($property as $row) {
                                    echo "<tr>";
                                    echo "<td>" . $row->property_no . "</td>";
                                    echo "<td>" . $row->property_title_en . "</td>";
                                    echo "<td>" . $row->price . "</td>";
                                    echo "<td>" . $row->available_date . "</td>";
                                    echo "<td>" . $row->status . "</td>";
                                    echo "<td><a href=\"" . base_url() . "index.php/renteasy_admin/invest_controller/update/" . $row->property_no . "\">"
                                    . "<button class=\"btn-success\">Update</button></a></td>";
  echo "<td><a href=\"" . base_url() . "index.php/invest_item_controller/index/" . $row->property_no . "\">"
                                    . "<button class=\"btn-info\">Details</button></a></td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->



<!-- modal-->
<div class="modal" id="addModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" style="color:gold">
                    Add A Invest property
                </h4>
            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/invest_controller/invest_process"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="addresslabel" class="control-label">Address:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="address" type="text" class="form-control" id="address" placeholder="Address">
                        </div>
                    </div>
                    
                    
                             <div class="form-group">
                        <div class="col-sm-3">
                            <label for="latitudelabel" class="control-label">Latitude:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="latitude" type="text" class="form-control" id="latitude" placeholder="latitude">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="longitudelabel" class="control-label">Longitude:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="longitude" type="text" class="form-control" id="longitude" placeholder="longitude">
                        </div>
                    </div>




                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="available_date" class="control-label">Available Date:</label>
                        </div>
                        <div class="col-sm-9">

                            <select name="year">
                                <?php
                                for ($i = 2015; $i < 2022; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>/
                            <select name="month">
                                <?php
                                for ($i = 1; $i < 13; $i++) {
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>/
                            <select name="day">
                                <?php
                                for ($i = 1; $i < 31; $i++) {
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="price" class="control-label">Price</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="price" name="price" min="0" >   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="type" class="control-label">Property Type:</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="type" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_title_en" class="control-label">Property title(English):</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="property_title_en" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_title_ch" class="control-label">Property title(Chinese):</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="property_title_ch" class="form-control"> 
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_tag_en" class="control-label">Property Tags(English):</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="property_tag_en" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_tag_ch" class="control-label">Property Tags(Chinese,separated by comma):</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="property_tag_ch" class="form-control"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_content_en" class="control-label">Content(English,separated by comma):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="others_en" name="property_content_en"></textarea>   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_content_ch" class="control-label">Content(Chinese):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="others_ch" name="property_content_ch"  >   
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="uploadfile" class="control-label">Images(must less than 1MB,and size 1500*1000,with jpg extension)</label>
                        </div>
                        <div class='col-sm-9'>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i1" name="i1"  >   
                            </div>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i2" name="i2"  >   
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i3" name="i3"  >   
                            </div>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i4" name="i4"  >   
                            </div>
                        </div>

                    </div>
                    <br/>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!--modal-->


<!--change property status modal-->
<div class="modal" id="changeModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" >
                    Change the status of the property
                </h4>

            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/invest_controller/property_change"
                      enctype="multipart/form-data">

                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="status" class="control-label">Status:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="status"  class='form-control'>

                                <option value="available">available</option>
                                <option value="done"  >done</option>
                                <option value="pending">pending</option>
                            </select>
                        </div>
                    </div>

                    <br/>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>



<!--change property feature modal-->
<div class="modal" id="featureModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" >
                    Change featured property
                </h4>

            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/general_controller/feature_property"
                      enctype="multipart/form-data">
                    <input type="hidden" name="type" value="<?php echo $_SESSION['admin_page'];?>">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no_1" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no_1' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no_2" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no_2' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>  <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no_3" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no_3' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->

</div>
<!-- /#wrapper -->

