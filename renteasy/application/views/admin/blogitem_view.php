
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Blog <small>property_no:<?php echo $blog_no; ?></small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Update Blog </h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" 
                              action="<?php echo base_url(); ?>index.php/renteasy_admin/blog_controller/blog_update"
                              enctype="multipart/form-data">
                            <div class="from-group">
                                    <input type="hidden" value="<?php echo $blog_no;?>" name="blog_no">
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="blog_title_en" class="control-label">blog tag:</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="blog_tag">
                                        <option value="rent" <?php
                                        if ($blog_tag == "rent") {
                                            echo 'selected';
                                        }
                                        ?>>rent</option>
                                        <option value="buy" <?php
                                        if ($blog_tag == "buy") {
                                            echo 'selected';
                                        }
                                        ?>>buy</option>
                                        <option value="invest"
                                        <?php
                                        if ($blog_tag == "invest") {
                                            echo 'selected';
                                        }
                                        ?>>invest</option>
                                        <option value="others" <?php 
                                        if($blog_tag=="others"){
                                            echo 'selected';
                                        }
                                        ?>>others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="blog_title_en" class="control-label">blog title(English):</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="blog_title_en" type="text" class="form-control" value="<?php echo $blog_title_en; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="blog_title_ch" class="control-label">blog title(Chinese):</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="blog_title_ch" type="text" class="form-control" value="<?php echo $blog_title_ch; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="blog_content_en" class="control-label">blog content(English):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea name="blog_content_en" class="form-control">
                                        <?php echo $blog_content_en; ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="blog_content_ch" class="control-label">blog content(Chinese):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea name="blog_content_ch" class="form-control">
                                        <?php echo $blog_content_ch; ?>

                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" col-sm-12 container">
                                    <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->





</div>
<!-- /#wrapper -->

