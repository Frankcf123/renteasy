
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span style='color:saddlebrown'>Blog</span> 
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-6 col-md-12" style="text-align: center">
                <a data-toggle="modal" 
                   data-target="#addModal" >
                    <button class="btn btn-primary btn-info btn-lg btn-block " >Add a Blog</button>
                </a>
            </div>

        </div>
        <br/>        <br/>
        <br/>

        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-bar-chart-o fa-fw"></i>All Blogs:<?php echo $count;?> </h3>
                    </div>
                    <div class="panel-body">
                        <!--<div id="morris-area-chart"></div>-->
                        <table class="container table table-striped">
                            <thead>
                            <th>Blog No</th>
                            <th>Tags:</th>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($blogs as $row) {
                                    echo "<tr>";
                                    echo "<td>" . $row->blog_no . "</td>";
                                    echo "<td>" . $row->blog_tag . "</td>";
                                    echo "<td>" . $row->blog_title_en . "</td>";
                                    echo "<td><a href=\"" . base_url() . "index.php/renteasy_admin/blog_controller/update/" . $row->blog_no . "\">"
                                    . "<button class=\"btn-success\">Update</button></a></td>";
                                    echo "<td><a href=\"" . base_url() . "index.php/renteasy_admin/blog_controller/delete/" . $row->blog_no . "\">"
                                    . "<button class=\"btn-danger\">Delete</button></a></td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->



<!-- modal-->
<div class="modal" id="addModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" style="color:gold">
                    Add A Blog
                </h4>
            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/blog_controller/blog_process"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="blog_title_en" class="control-label">blog tag:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="blog_tag">
                                <option value="rent">rent</option>
                                <option value="buy">buy</option>
                                <option value="invest">invest</option>
                                <option value="others">others</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="blog_title_en" class="control-label">blog title(English):</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="blog_title_en" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="blog_title_ch" class="control-label">blog title(Chinese):</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="blog_title_ch" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="blog_content_en" class="control-label">blog content(English):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea name="blog_content_en" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="blog_content_ch" class="control-label">blog content(Chinese):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea name="blog_content_ch" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!--modal-->






</div>
<!-- /#wrapper -->

