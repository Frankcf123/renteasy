
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Rent <small>add,delete,update</small>
                </h1>
                <!--                        <ol class="breadcrumb">
                                            <li class="active">
                                                <i class="fa fa-dashboard"></i> Dashboard
                                            </li>
                                        </ol>-->
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-4 col-md-12" >
                <a data-toggle="modal" 
                   data-target="#addModal" >
                    <button class="btn btn-primary btn-info btn-lg " >Add Property</button>
                </a>
            </div>
            <div class="col-lg-4 col-md-12"  >
                <a data-toggle="modal" 
                   data-target="#changeModal" >
                    <button class="btn btn-primary btn-info btn-lg " >Change Property Status</button>
                </a>

            </div>   
             <div class="col-lg-4 col-md-12"  >
                <a data-toggle="modal" 
                   data-target="#featureModal" >
                    <button class="btn btn-primary btn-info" >Change Featured property</button>
                </a>
            </div>      
        </div>
        <br/>        <br/>
        <br/>

        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-bar-chart-o fa-fw"></i>All Properties(<?php echo $count; ?>) </h3>
                    </div>
                    <div class="panel-body">
                        <!--<div id="morris-area-chart"></div>-->
                        <table class="container table table-striped">
                            <thead>
                            <th>Property_No</th>
                            <th>Title</th>
                            <th>Available Date:</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($property as $row) {
                                    echo "<tr>";
                                    echo "<td>" . $row->property_no . "</td>";
                                    echo "<td>" . $row->property_title_en . "</td>";
                                    echo "<td>" . $row->available_date . "</td>";
                                    echo "<td>" . $row->status . "</td>";
                                    echo "<td><a href=\"" . base_url() . "index.php/renteasy_admin/rent_controller/update/" . $row->property_no . "\">"
                                    . "<button class=\"btn-success\">Update</button></a></td>";
                                    echo "<td><a href=\"" . base_url() . "index.php/rent_item_controller/index/" . $row->property_no . "\">"
                                    . "<button class=\"btn-info\">Details</button></a></td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->



<!-- modal-->
<div class="modal" id="addModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" style="color:gold">
                    Add A Rent property
                </h4>
            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/rent_controller/rent_process"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="rentlabel" class="control-label">Title English:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="property_title_en" type="text" class="form-control" placeholder="Title english">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="rentlabel" class="control-label">Tile Chinese:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="property_title_ch" type="text" class="form-control" placeholder="Title Chinese">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="rentlabel" class="control-label">Address:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="address" type="text" class="form-control" placeholder="Address">
                        </div>
                    </div>
                
         <div class="form-group">
                        <div class="col-sm-3">
                            <label for="latitudelabel" class="control-label">Latitude:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="latitude" type="text" class="form-control" id="latitude" placeholder="latitude">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="longitudelabel" class="control-label">Longitude:</label>
                        </div>
                        <div class="col-sm-9">
                            <input name="longitude" type="text" class="form-control" id="longitude" placeholder="longitude">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="available_date" class="control-label">Available Date:</label>
                        </div>
                        <div class="col-sm-9">

                            <select name="year">
                                <?php
                                for ($i = 2015; $i < 2022; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>/
                            <select name="month">
                                <?php
                                for ($i = 1; $i < 13; $i++) {
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>/
                            <select name="day">
                                <?php
                                for ($i = 1; $i < 31; $i++) {
                                    if ($i < 10) {
                                        $i = '0' . $i;
                                    }
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="bedroom" class="control-label">Bedroom:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="bedroom">
                                <?php
                                for ($i = 0; $i < 10; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="bathroom" class="control-label">Bathroom:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="bathroom">
                                <?php
                                for ($i = 0; $i < 10; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="garage" class="control-label">Garage</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="garage">
                                <?php
                                for ($i = 0; $i < 10; $i++) {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php }
                                ?>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="price" class="control-label">Price</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="price" name="price" min="0" >   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="suburb" class="control-label">Suburb:(All upper case)</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="suburb" name="suburb">   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="uni" class="control-label">Uni Nearby</label>
                        </div>
                        <div class="col-sm-9">

                            <div class="checkbox"><label><input type="checkbox" name="rmit">RMIT</label>
                            </div>
                            <div class="checkbox"><label><input  type="checkbox" name="mel_uni">Melbourne Uni</label>
                            </div>
                            <div class="checkbox"><label><input type="checkbox" name="monash_uni">Monash Uni</label>
                            </div>
                            <div class="checkbox"><label><input type="checkbox" name="latrobe_uni">Latrobe Uni</label>
                            </div>
                            <div class="checkbox"><label><input type="checkbox" name="swinburne_uni">Swinburne Uni</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="bus" class="control-label">Bus No:(separate by comma)</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="tram" name="bus"  >   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="tram" class="control-label">Tram No:(separate by comma)</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="tram" name="tram"  >   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="train" class="control-label">Train station:</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="tram" name="train"  >   
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="facility_nearby_en" class="control-label">Facility Nearby(English):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="facility_nearby_en" name="facility_nearby_en"></textarea>   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="facility_nearby_ch" class="control-label">Facility Nearby(Chinese):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="facility_nearby_ch" name="facility_nearby_ch"  >   
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="others_en" class="control-label">Others(English):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="others_en" name="others_en"></textarea>   
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="others_ch" class="control-label">Others(Chinese):</label>
                        </div>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="others_ch" name="others_ch"  >   
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="uploadfile" class="control-label">Images(must less than 1m,and size 1500*1000,with jpg extension)</label>
                        </div>
                        <div class='col-sm-9'>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i1" name="i1"  >   
                            </div>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i2" name="i2"  >   
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i3" name="i3"  >   
                            </div>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" id="i4" name="i4"  >   
                            </div>
                        </div>

                    </div>
                    <br/>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!--modal-->


<!--change property status modal-->
<div class="modal" id="changeModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" >
                    Change the status of the property
                </h4>

            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/rent_controller/property_change"
                      enctype="multipart/form-data">

                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="status" class="control-label">Status:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="status"  class='form-control'>

                                <option value="available">available</option>
                                <option value="done"  >done</option>
                                <option value="pending">pending</option>
                            </select>
                        </div>
                    </div>

                    <br/>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>


<!--change property feature modal-->
<div class="modal" id="featureModal" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >

                <h4 class="modal-title" id="myModalLabel" >
                    Change featured property
                </h4>

            </div>
            <div class="modal-body" >
                <form class="form-horizontal" role="form" method="post" 
                      action="<?php echo base_url(); ?>index.php/renteasy_admin/general_controller/feature_property"
                      enctype="multipart/form-data">
                    <input type="hidden" name="type" value="<?php echo $_SESSION['admin_page'];?>">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no_1" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no_1' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no_2" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no_2' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>  <div class="form-group">
                        <div class="col-sm-3">
                            <label for="property_no_3" class="control-label">Property No:</label>
                        </div>
                        <div class="col-sm-9">
                            <select name='property_no_3' class='form-control'>
                                <?php
                                foreach ($property as $row) {
                                    echo "<option value=" . $row->property_no . ">" . $row->property_no . "</option>";
                                }
                                ?>   
                            </select>                       </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class=" col-sm-12 container">
                            <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: black">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->


</div>
<!-- /#wrapper -->

