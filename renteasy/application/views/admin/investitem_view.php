
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Invest <small>property_no:<?php echo $property_no; ?></small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Update Property </h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" 
                              action="<?php echo base_url(); ?>index.php/renteasy_admin/invest_controller/invest_update"
                              enctype="multipart/form-data">
                            <div class="form-group">

                                <div class="col-sm-9">
                                    <input name="property_no" type="hidden" class="form-control" 
                                           value='<?php echo $property_no; ?>' >

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="addresslabel" class="control-label">Address:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="address" type="text" class="form-control" id="address" 
                                           value="<?php echo $address; ?>">
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="latitudelabel" class="control-label">Latitude:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="latitude" type="text" value="<?php echo $latitude;?>" 
                                           class="form-control" id="latitude" placeholder="latitude">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="longitudelabel" class="control-label">Longitude:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="longitude" type="text" class="form-control"
                                           value="<?php echo $longitude;?>" 
                                           id="longitude" placeholder="longitude">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="status" class="control-label">Status:</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="status">

                                        <option value="available"   <?php if ($status == "available") echo "selected=\'selected\'"; ?>
                                                >available</option>
                                        <option value="done"   <?php if ($status == "done") echo "selected=\'selected\'"; ?>
                                                >done</option>
                                        <option value="pending"   <?php if ($status == "pending") echo "selected=\'selected\'"; ?>
                                                >pending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="available_date" class="control-label">Available Date:</label>
                                </div>
                                <div class="col-sm-9">

                                    <select name="year">
                                        <?php
                                        for ($i = 2015; $i < 2022; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $year) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>


                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>/
                                    <select name="month">
                                        <?php
                                        for ($i = 1; $i < 13; $i++) {
                                            if ($i < 10) {
                                                $i = '0' . $i;
                                            }
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $month) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>
                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>/
                                    <select name="day">
                                        <?php
                                        for ($i = 1; $i < 31; $i++) {
                                            if ($i < 10) {
                                                $i = '0' . $i;
                                            }
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $day) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>
                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="price" class="control-label">Price</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="price" class="form-control" value="<?php echo $price; ?>">   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="type" class="control-label">Property Type:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="type" class="form-control" value="<?php echo $type?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="property_title_en" class="control-label">Property title(English):</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="property_title_en" class="form-control" value="<?php echo $property_title_en ?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="property_title_ch" class="control-label">Property title(Chinese):</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="property_title_ch" class="form-control"  value="<?php echo $property_title_ch ?>"> 
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="property_tag_en" class="control-label">Tags(English,separated by comma):</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="property_tag_en" class="form-control"  value="<?php echo $property_tag_en ?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="property_tag_ch" class="control-label">Tags(Chinese,separated by comma):</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="property_tag_ch" class="form-control"  value="<?php echo $property_tag_ch ?>"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="property_content_en" class="control-label">Content(English):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="others_en" name="property_content_en">
                                        <?php echo $property_content_en ?>
                                    </textarea>   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="property_content_ch" class="control-label">Content(Chinese):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="others_ch" name="property_content_ch"  >   
                                        <?php echo $property_content_ch ?>

                                    </textarea>
                                </div>
                            </div>

                            <br/>
                            <div class="form-group">
                                <div class=" col-sm-12 container">
                                    <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->





</div>
<!-- /#wrapper -->

