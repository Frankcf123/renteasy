
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
<!--                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>-->
                <a class="navbar-brand" href="<?php echo base_url(); ?>index.php"> Rent Home</a>
            </div>
            <?php
            if (!isset($_SESSION)) {
                session_start();
            }
            ?>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?php if ($_SESSION['admin_page'] == 'index') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php/renteasy_admin/index_controller/index">
                            <i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="<?php if ($_SESSION['admin_page'] == 'rent') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php/renteasy_admin/rent_controller/index">
                            <i class="fa fa-fw fa-bar-chart-o"></i> Rent</a>
                    </li>
                    <li class="<?php if ($_SESSION['admin_page'] == 'buy') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php/renteasy_admin/buy_controller/index">
                            <i class="fa fa-fw fa-table"></i> Buy</a>
                    </li>
                    <li class="<?php if ($_SESSION['admin_page'] == 'invest') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php/renteasy_admin/invest_controller/index">
                            <i class="fa fa-fw fa-edit"></i> Invest</a>
                    </li>
                    <li class="<?php if ($_SESSION['admin_page'] == 'blog') echo 'active'; ?>">
                        <a href="<?php echo base_url(); ?>index.php/renteasy_admin/blog_controller/index">
                            <i class="fa fa-fw fa-recycle"></i> Blog</a>
                    </li>




                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
