
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Buy <small>property_no:<?php echo $property_no; ?></small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Update Property </h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" 
                              action="<?php echo base_url(); ?>index.php/renteasy_admin/buy_controller/buy_update"
                              enctype="multipart/form-data">
                            <div class="form-group">

                                <div class="col-sm-9">
                                    <input name="property_no" type="hidden" class="form-control"
                                           value='<?php echo $property_no; ?>' >

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="titlelabel" class="control-label">Title Chinese:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="property_title_ch" type="text" class="form-control" 
                                           value="<?php echo $property_title_ch; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="titlelabel" class="control-label">Title English:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="property_title_en" type="text" class="form-control" 
                                           value="<?php echo $property_title_en; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="addresslabel" class="control-label">Address:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="address" type="text" class="form-control" id="address" 
                                           value="<?php echo $address; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="latitudelabel" class="control-label">Latitude:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="latitude" type="text" value="<?php echo $latitude;?>" 
                                           class="form-control" id="latitude" placeholder="latitude">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="longitudelabel" class="control-label">Longitude:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input name="longitude" type="text" class="form-control"
                                           value="<?php echo $longitude;?>" 
                                           id="longitude" placeholder="longitude">
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="status" class="control-label">Status:</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="status">

                                        <option value="available"   <?php if ($status == "available") echo "selected=\'selected\'"; ?>
                                                >available</option>
                                        <option value="done"   <?php if ($status == "done") echo "selected=\'selected\'"; ?>
                                                >done</option>
                                        <option value="pending"   <?php if ($status == "pending") echo "selected=\'selected\'"; ?>
                                                >pending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="available_date" class="control-label">Available Date:</label>
                                </div>
                                <div class="col-sm-9">

                                    <select name="year">
                                        <?php
                                        for ($i = 2015; $i < 2022; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $year) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>


                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>/
                                    <select name="month">
                                        <?php
                                        for ($i = 1; $i < 13; $i++) {
                                            if ($i < 10) {
                                                $i = '0' . $i;
                                            }
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $month) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>
                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>/
                                    <select name="day">
                                        <?php
                                        for ($i = 1; $i < 31; $i++) {
                                            if ($i < 10) {
                                                $i = '0' . $i;
                                            }
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $day) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>
                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="bedroom" class="control-label">Bedroom:</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="bedroom">
                                        <?php
                                        for ($i = 0; $i < 10; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>" <?php
                                            if ($i == $bedroom) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="bathroom" class="control-label">Bathroom:</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="bathroom">
                                        <?php
                                        for ($i = 0; $i < 10; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $bathroom) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>
                                                    ><?php echo $i; ?></option>
                                                <?php }
                                                ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="garage" class="control-label">Garage</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="garage">
                                        <?php for ($i = 0; $i < 10; $i++) { ?>

                                            <option value="<?php echo $i; ?>"
                                            <?php
                                            if ($i == $garage) {
                                                echo "selected=\'selected\'";
                                            }
                                            ?>
                                                    ><?php echo $i; ?></option>
                                                <?php } ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="price" class="control-label">Price</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="price" name="price" 
                                           value="<?php echo $price; ?>">   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="suburb" class="control-label">Suburb:(All upper case)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="suburb" name="suburb"
                                           >   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="uni" class="control-label">Uni Nearby</label>
                                </div>

                                <div class="col-sm-9">
                                    <div class="checkbox"><label><input name="rmit" type="checkbox" 
                                            <?php
                                            if (preg_match('/.*rmit.*/', $uni_nearby)) {
                                                echo 'checked ';
                                            }
                                            ?>
                                                                        >RMIT</label>
                                    </div>
                                    <div class="checkbox"><label><input name="mel_uni" type="checkbox"
                                            <?php
                                            if (preg_match('/.*mel_uni.*/', $uni_nearby)) {
                                                echo 'checked ';
                                            }
                                            ?>>Melbourne Uni</label>
                                    </div>
                                    <div class="checkbox"><label><input type="checkbox" name="monash_uni"
                                            <?php
                                            if (preg_match('/.*monash_uni.*/', $uni_nearby)) {
                                                echo 'checked ';
                                            }
                                            ?>>Monash Uni</label>
                                    </div>
                                    <div class="checkbox"><label><input type="checkbox" name="latrobe_uni"
                                            <?php
                                            if (preg_match('/.*latrobe_uni.*/', $uni_nearby)) {
                                                echo 'checked ';
                                            }
                                            ?>>Latrobe Uni</label>
                                    </div>
                                    <div class="checkbox"><label><input type="checkbox" name="swinburne_uni"
                                            <?php
                                            if (preg_match('/.*swinburne_uni.*/', $uni_nearby)) {
                                                echo 'checked ';
                                            }
                                            ?>>Swinburne Uni</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="bus" class="control-label">Bus No:(separate by comma)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="tram" name="bus"  
                                           value="<?php echo $bus_nearby; ?>">   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="tram" class="control-label">Tram No:(separate by comma)</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="tram" name="tram"  
                                           value="<?php echo $tram_nearby; ?>">   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="train" class="control-label">Train station:</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="tram" name="train" 
                                           value="<?php echo $train_station_nearby ?>">   
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="facility_nearby_en" class="control-label">Facility Nearby(English):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="facility_nearby_en" name="facility_nearby_en">
                                        <?php echo $facility_nearby_en; ?>
                                    </textarea>   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="facility_nearby_ch" class="control-label">Facility Nearby(Chinese):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="facility_nearby_ch" name="facility_nearby_ch"  >   
                                        <?php echo $facility_nearby_ch; ?>

                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="others_en" class="control-label">Others(English):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="others_en" name="others_en">
                                        <?php echo $others_en; ?></textarea>   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="others_ch" class="control-label">Others(Chinese):</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="others_ch" name="others_ch"  >   
                                        <?php echo $others_ch; ?>
                                    </textarea>
                                </div>
                            </div>

                            <br/>
                            <div class="form-group">
                                <div class=" col-sm-12 container">
                                    <input type="submit" class="btn btn-block btn-lg btn-info" value="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->





</div>
<!-- /#wrapper -->

