<br/><br/><br/>

<br/><br/>
---------------------------------------------


<!-- Page Heading/Breadcrumbs -->

<div class="container col-lg-12">



    <!--page divider-->
    <div class="col-md-12">
        <div id="carousel-example-generic" data-interval="false" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?php echo base_url(); ?>/upload_files/feature_images/feature_image_1.jpg" 
                         style="width:1200px;height:300px;">
                    <div class="carousel-caption">
                        <a href=""><h2><?php echo lang('melbourne')?></h2></a>
                    </div>
                </div>
                <div class="item ">
                    <img src="<?php echo base_url(); ?>/upload_files/feature_images/feature_image_2.jpg" 
                         style="width:1200px;height:300px;">
                    <div class="carousel-caption">
                        <a href=""><h2><?php echo lang('melbourne')?></h2></a>
                    </div>
                </div>
                <div class="item ">
                    <img src="<?php echo base_url(); ?>/upload_files/feature_images/feature_image_3.jpg" 
                         style="width:1200px;height:300px;">
                    <div class="carousel-caption">
                        <a href=""><h2><?php echo lang('melbourne')?></h2></a>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span></a></div>
        <h1 class="page-header" style="color:#80720B">
            <?php echo lang('search_result'); ?>:
            <small >result sdjfjs</small>
        </h1>


    </div>


    <!--    <div class="col-md-12">
                            <h1 class="page-header" style="color:yellow">Contact us
            
                            </h1>
    
    
            <ol class="breadcrumb" style="background:#BFAF9D">
    
                <li>
                    <a href="<?php echo base_url() . 'index.php/home_controller/index' ?>">
    <?php echo lang('home'); ?>  </a>
                </li>
    
                <li>
    <?php echo lang('search_result'); ?>  
                </li>
    
    
            </ol>
        </div>-->
    <?php
    if (!isset($_SESSION)) {
        session_start();
    }
    $_SESSION['tab'] = 'rent';
    ?>

    <!--the details-->
    <div class="container col-md-12 col-lg-12 col-sm-12" style="align:center">
        <div id="myTab"  class="nav nav-tabs col-md-12" draggable="true" style="text-align: center;">
            <div class="btn-group">
                <a id="renttab" href="#rent" data-toggle="tab" class="btn-info btn btn-lg btn-default"><?php echo lang('rent'); ?></a>
                <a id="buytab" href="#buy" data-toggle="tab" class="btn btn-lg btn-default"><?php echo lang('buy'); ?></a>
                <a id="investtab" href="#invest" data-toggle="tab" class="btn  btn-lg btn-default"><?php echo lang('invest'); ?></a>
            </div>

        </div>

        <script>

            var rent = document.getElementById("renttab");
            var buy = document.getElementById("buytab");
            var invest = document.getElementById("investtab");

            rent.onclick = function() {
                rent.className = "btn-info btn btn-lg btn-default";
                buy.className = " btn btn-lg btn-default";
                invest.className = " btn btn-lg btn-default";

            };
            buy.onclick = function() {
                buy.className = "btn-info btn btn-lg btn-default";
                rent.className = " btn btn-lg btn-default";
                invest.className = " btn btn-lg btn-default";

            };
            invest.onclick = function() {
                invest.className = "btn-info btn btn-lg btn-default";
                buy.className = " btn btn-lg btn-default";
                rent.className = " btn btn-lg btn-default";

            };
        </script>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="rent">
                <br/><br/><br/><br/>
                <?php foreach ($rent_rows as $row) { ?>
                    <div class="panel panel-primary">
                        <?php
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        //
                        $lan = $_SESSION['lang'];
                        ?>
                        <div class="panel-heading pull" style="background: #F2F2EB">
                            <span class="pull-right">
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div id="slide_rent_<?php echo $row->property_no; ?>" data-interval="false" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <a href="<?php echo base_url(); ?>index.php/rent_item_controller/index/<?php echo $row->property_no; ?>">
                                                <img 
                                                    src="<?php echo base_url(); ?>/upload_files/rent_images/rent_<?php echo $row->property_no; ?>_1.jpg" 
                                                    class="img-thumbnail img-responsive img-centered"
                                                    ></a>
                                        </div>
                                        <?php for ($i = 1; $i < 5; $i++) { ?>
                                            <div class="item ">
                                                <a href="<?php echo base_url(); ?>index.php/rent_item_controller/index/<?php echo $row->property_no; ?>">
                                                    <img 
                                                        src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $row->latitude . "," . $row->longitude; ?>&zoom=11&size=400x263&sensor=false"
                                                        class="img-thumbnail img-responsive img-centered" width="400" height="280"
                                                        ></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class="left carousel-control" href="#slide_rent_<?php echo $row->property_no; ?>" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span></a>
                                    <a class="right carousel-control" href="#slide_rent_<?php echo $row->property_no; ?>" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h4 style="color:goldenrod;text-align: center">
                                    <a style="color:goldenrod;" href="<?php echo base_url(); ?>index.php/rent_item_controller/index/<?php echo $row->property_no; ?>">
                                        <?php
                                        if ($lan == 'english') {
                                            echo $row->property_title_en;
                                        } else {
                                            echo $row->property_title_ch;
                                        }
                                        ?></a></h4>
                                <p style="color:goldenrod;text-align: center">  
                                <ul class="list-inline mrg-0 btm-mrg-10 clr-535353" style="text-align: center" >
                                    <li><strong>$<?php echo $row->price; ?></strong>/<?php echo lang('week'); ?> </li>
                                    <li style="list-style: none">|</li>
                                    <li><strong><?php echo $row->bedroom; ?></strong> <?php echo lang('bedroom'); ?> </li>
                                    <li style="list-style: none">|</li>
                                    <li><strong><?php echo $row->garage; ?></strong> <?php echo lang('garage'); ?> </li>
                                    <li style="list-style: none">|</li>
                                    <li><strong><?php echo $row->bathroom; ?></strong> <?php echo lang('bathroom'); ?> </li>
                                    <li style="list-style: none">|</li>

                                </ul>
                                <br/>

                                <h4 style="color:black;text-align: center" class="pull-left">
                                    <?php echo lang('address'); ?>:
                                    <a  style="color:gray" href="<?php echo base_url(); ?>index.php/rent_item_controller/index/<?php echo $row->property_no; ?>">
                                        <?php echo $row->address; ?></a></h4>
                                <br/>
                                </p>
                                <hr>

                                <a class="btn btn-primary btn-lg btn-block" 
                                   href="<?php echo base_url(); ?>index.php/rent_item_controller/index/<?php echo $row->property_no; ?>">
                                    <?php echo lang('learn_more'); ?></a>
                            </div>
                        </div>
                    </div> 
                <?php } ?>
            </div>
            <div class="tab-pane fade" id="buy">
                <br/><br/><br/><br/>
                <?php foreach ($buy_rows as $row) { ?>
                    <div class="panel panel-primary">
                        <?php
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        //
                        $lan = $_SESSION['lang'];
                        ?>
                        <div class="panel-heading pull" style="background: #F2F2EB">
                            <span class="pull-right">
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div id="slide_buy_<?php echo $row->property_no; ?>" data-interval="false" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                                <img 
                                                    <img 
                                                    src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $row->latitude . "," . $row->longitude; ?>&zoom=11&size=400x263&sensor=false"
                                                    class="img-thumbnail img-responsive img-centered" width="400" height="280"
                                                    ></a>
                                        </div>
                                        <?php for ($i = 1; $i < 5; $i++) { ?>
                                            <div class="item ">
                                                <a href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                                    <img 
                                                        src="<?php echo base_url(); ?>/upload_files/buy_images/buy_<?php echo $row->property_no; ?>_<?php echo $i; ?>.jpg" 
                                                        class="img-thumbnail img-responsive img-centered"  width="400" height="280"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class="left carousel-control" href="#slide_buy_<?php echo $row->property_no; ?>" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span></a>
                                    <a class="right carousel-control" href="#slide_buy_<?php echo $row->property_no; ?>" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h4 style="color:goldenrod;text-align: center">
                                    <a style="color:goldenrod;" href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                        <?php
                                        if ($lan == 'english') {
                                            echo $row->property_title_en;
                                        } else {
                                            echo $row->property_title_ch;
                                        }
                                        ?></a></h4>
                                <p style="color:goldenrod;text-align: center">  
                                <ul class="list-inline mrg-0 btm-mrg-10 clr-535353" style="text-align: center" >
                                    <li><strong>$<?php echo $row->price; ?></strong> </li>
                                    <li style="list-style: none">|</li>
                                    <li><strong><?php echo $row->bedroom; ?></strong> <?php echo lang('bedroom'); ?> </li>
                                    <li style="list-style: none">|</li>
                                    <li><strong><?php echo $row->garage; ?></strong> <?php echo lang('garage'); ?> </li>
                                    <li style="list-style: none">|</li>
                                    <li><strong><?php echo $row->bathroom; ?></strong> <?php echo lang('bathroom'); ?> </li>
                                    <li style="list-style: none">|</li>

                                </ul>
                                <br/>

                                <h4 style="color:black;text-align: center" class="pull-left">
                                    <?php echo lang('address'); ?>:
                                    <a  style="color:gray" href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                        <?php echo $row->address; ?></a></h4>
                                <br/>
                                </p>
                                <hr>

                                <a class="btn btn-primary btn-lg btn-block" 
                                   href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                    <?php echo lang('learn_more'); ?></a>
                            </div>
                        </div>
                    </div> 
                <?php } ?>   
            </div>
            <div class="tab-pane fade" id="invest">
                <br/><br/><br/><br/>

                <?php foreach ($invest_rows as $row) { ?>
                    <div class="panel panel-primary">

                        <div class="panel-heading pull" style="background: #F2F2EB">
                            <span class="pull-right">
                            </span>
                        </div>
                        <?php
                        if (!isset($_SESSION)) {
                            session_start();
                        }
                        //
                        $lan = $_SESSION['lang'];
                        ?>
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div id="slide_invest_<?php echo $row->property_no; ?>" data-interval="false" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <a href="<?php echo base_url(); ?>index.php/invest_item_controller/index/<?php echo $row->property_no; ?>">
                                                <img 
                                                    src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $row->latitude . "," . $row->longitude; ?>&zoom=11&size=400x263&sensor=false"
                                                    class="img-thumbnail img-responsive img-centered" width="400" height="280"
                                                    ></a>
                                        </div>
                                        <?php for ($i = 1; $i < 5; $i++) { ?>
                                            <div class="item ">
                                                <a href="<?php echo base_url(); ?>index.php/invest_item_controller/index/<?php echo $row->property_no; ?>">
                                                    <img 
                                                        src="<?php echo base_url(); ?>/upload_files/invest_images/invest_<?php echo $row->property_no; ?>_<?php echo $i; ?>.jpg" 
                                                        class="img-thumbnail img-responsive img-centered"  width="400" height="280"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class="left carousel-control" href="#slide_invest_<?php echo $row->property_no; ?>" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span></a>
                                    <a class="right carousel-control" href="#slide_invest_<?php echo $row->property_no; ?>" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h4 style="color:goldenrod;text-align: center">
                                    <a href="<?php echo base_url(); ?>index.php/invest_item_controller/index/<?php echo $row->property_no; ?>">
                                        <?php
                                        if ($lan = 'english') {
                                            echo $row->property_title_en;
                                        } else {
                                            echo $row->property_title_ch;
                                        }
                                        ?>

                                    </a></h4>
                                <p style="color:goldenrod;text-align: center">  
                                <ul class="list-inline mrg-0 btm-mrg-10 clr-535353" style="text-align: center" >
                                    <?php
                                    if ($lan == 'english') {
                                        $tags = $row->property_tag_en;
                                    } else {
                                        $tags = $row->property_tag_ch;
                                    }

                                    $tag_arr = preg_split('[\,]', $tags);
                                    for ($i = 0; $i < sizeof($tag_arr); $i++) {
                                        ?>
                                        <li><button class="btn btn-primary" style="background-color:background">
                                                <?php echo $tag_arr[$i]; ?></button>
                                        </li>
                                        <li style="list-style: none">|</li>
                                    <?php } ?>


                                </ul>
                                <br/>

                                <h4 style="color:black;text-align: center" class="pull-left">
                                    <?php echo lang('address'); ?>:
                                    <a  style="color:gray" href="<?php echo base_url(); ?>index.php/buy_item_controller/index/<?php echo $row->property_no; ?>">
                                        <?php echo $row->address; ?></a></h4>
                                <br/>
                                </p>
                                <hr>
                                <a class="btn btn-primary btn-lg btn-block" 
                                   href="<?php echo base_url(); ?>index.php/invest_item_controller/index/<?php echo $row->property_no; ?>">
                                    <?php echo lang('learn_more'); ?></a>
                            </div>
                        </div>
                    </div> 
                <?php } ?>
            </div>

        </div>



    </div>

