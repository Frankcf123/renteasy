-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2015 at 04:01 PM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `renteasy`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_no` int(11) NOT NULL AUTO_INCREMENT,
  `blog_title_en` varchar(75) NOT NULL,
  `blog_title_ch` varchar(75) CHARACTER SET gbk NOT NULL,
  `blog_tag` varchar(75) NOT NULL,
  `blog_content_en` text NOT NULL,
  `blog_content_ch` text CHARACTER SET gbk NOT NULL,
  PRIMARY KEY (`blog_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_no`, `blog_title_en`, `blog_title_ch`, `blog_tag`, `blog_content_en`, `blog_content_ch`) VALUES
(1, 'dfas', 'sdf', 'rent', '                                        asdf                                    ', '                                        sdfas\n                                    ');

-- --------------------------------------------------------

--
-- Table structure for table `property_buy`
--

CREATE TABLE `property_buy` (
  `property_no` int(11) NOT NULL AUTO_INCREMENT,
  `property_title_en` text NOT NULL,
  `property_title_ch` text CHARACTER SET gbk NOT NULL,
  `address` text NOT NULL,
  `available_date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `bedroom` int(11) NOT NULL,
  `bathroom` int(11) NOT NULL,
  `garage` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `uni_nearby` text NOT NULL,
  `bus_nearby` int(11) NOT NULL,
  `tram_nearby` int(11) NOT NULL,
  `train_station_nearby` varchar(78) NOT NULL,
  `others_en` text NOT NULL,
  `others_ch` text CHARACTER SET gbk NOT NULL,
  `suburb` varchar(78) NOT NULL,
  `facility_nearby_en` text NOT NULL,
  `facility_nearby_ch` text CHARACTER SET gbk NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`property_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `property_buy`
--

INSERT INTO `property_buy` (`property_no`, `property_title_en`, `property_title_ch`, `address`, `available_date`, `status`, `bedroom`, `bathroom`, `garage`, `price`, `uni_nearby`, `bus_nearby`, `tram_nearby`, `train_station_nearby`, `others_en`, `others_ch`, `suburb`, `facility_nearby_en`, `facility_nearby_ch`, `latitude`, `longitude`) VALUES
(1, 'title en', '', 'asdas', '2015-01-01', 'available', 0, 0, 0, 829, 'swinburne_uni:', 1, 0, 'wqe', '                                                                                                                                                                                                                                                eqwe', '   \n                                           \n                                           \n                                           \n                                           \n                                           \n                                           qweqw\n                                                                                                                                                                                                                                                    ', '', '                                                                                                                                                                                                                                                qwe                                                                                                                                                                                                                        ', '   \n                                           \n                                           \n                                           \n                                           \n                                           \n                                           wqe\n                            \n                                    \n                                    \n                                    \n                                    \n                                    \n                                    ', -12, 32),
(2, 'fdsfds', '是否', 'dsa', '2015-01-01', 'available', 0, 0, 0, 1, 'mel_uni:', 0, 12, '232', '                                        23', ' 123                                                     ', 'CARLTION', '                                        32                                    ', '   \n                                           12\n                            \n                                    ', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `property_feature`
--

CREATE TABLE `property_feature` (
  `property_type` varchar(120) NOT NULL,
  `property_no_1` int(11) NOT NULL,
  `property_no_2` int(11) NOT NULL,
  `property_no_3` int(11) NOT NULL,
  PRIMARY KEY (`property_type`),
  UNIQUE KEY `property_type` (`property_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_feature`
--

INSERT INTO `property_feature` (`property_type`, `property_no_1`, `property_no_2`, `property_no_3`) VALUES
('buy', 1, 1, 1),
('invest', 1, 1, 1),
('rent', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `property_invest`
--

CREATE TABLE `property_invest` (
  `property_no` int(11) NOT NULL AUTO_INCREMENT,
  `property_title_en` varchar(100) NOT NULL,
  `property_title_ch` varchar(100) CHARACTER SET gbk NOT NULL,
  `property_content_en` text NOT NULL,
  `property_content_ch` text CHARACTER SET gbk NOT NULL,
  `property_tag_en` varchar(100) NOT NULL,
  `property_tag_ch` varchar(100) CHARACTER SET gbk NOT NULL,
  `address` text NOT NULL,
  `available_date` date NOT NULL,
  `price` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `type` varchar(90) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`property_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `property_invest`
--

INSERT INTO `property_invest` (`property_no`, `property_title_en`, `property_title_ch`, `property_content_en`, `property_content_ch`, `property_tag_en`, `property_tag_ch`, `address`, `available_date`, `price`, `status`, `type`, `latitude`, `longitude`) VALUES
(1, 'title en title', 'dsf', '                                        msdfm                                    ', '   \n                                        fmsd,   \n                            \n                                    ', 'sdf,dsad', 'few,fdsfa', 'fsdf', '2015-01-01', 12, 'available', '32', 321, 0);

-- --------------------------------------------------------

--
-- Table structure for table `property_rent`
--

CREATE TABLE `property_rent` (
  `property_no` int(11) NOT NULL AUTO_INCREMENT,
  `address` text NOT NULL,
  `available_date` date NOT NULL,
  `status` varchar(80) NOT NULL,
  `bedroom` int(11) NOT NULL,
  `bathroom` int(11) NOT NULL,
  `garage` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `uni_nearby` text NOT NULL,
  `bus_nearby` int(11) NOT NULL,
  `tram_nearby` int(11) NOT NULL,
  `train_station_nearby` varchar(78) NOT NULL,
  `others_en` text NOT NULL,
  `others_ch` text CHARACTER SET gbk NOT NULL,
  `suburb` varchar(78) NOT NULL,
  `facility_nearby_ch` text CHARACTER SET gbk NOT NULL,
  `facility_nearby_en` text NOT NULL,
  `property_title_en` text NOT NULL,
  `property_title_ch` text CHARACTER SET gbk NOT NULL,
  `latitude` int(11) NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`property_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `property_rent`
--

INSERT INTO `property_rent` (`property_no`, `address`, `available_date`, `status`, `bedroom`, `bathroom`, `garage`, `price`, `uni_nearby`, `bus_nearby`, `tram_nearby`, `train_station_nearby`, `others_en`, `others_ch`, `suburb`, `facility_nearby_ch`, `facility_nearby_en`, `property_title_en`, `property_title_ch`, `latitude`, `longitude`) VALUES
(1, '75 Flemington Road', '2015-01-01', 'available', 0, 0, 0, 43, 'mel_uni:monash_uni:latrobe_uni:', 0, 234, '324', '                                                                                                                        fdsfjsdlakfjsadlkfjsldkjfldskfjdslkvcx,mnvzxcmvn,', '   \n                                           \n                                           \n                                        中文版本中文欧式的回复就卡死的回复可见阿萨德飞回家收卡的分哈克说地方很少见的开发挥洒的f收到发货就死定了                                                                                                            ', '', '   \n                                           \n                                           \n                 asdf                       \n                                    \n                                    \n                                    ', '                                                                                                                                                            sdf                                                                        ', 'title', 'title chinese', 123, 0),
(2, 'gjfdklgjds', '2015-01-01', 'available', 0, 0, 0, 232, 'monash_uni:latrobe_uni:', 12, 123, 'station', 'fdsfjsdlakfjsadlkfjsldkjfldskfjdslkvcx,mnvzxcmvn,xzcnvcxmdsklfjlksdjfklsdjflkds jfslksdjflksdjflksdjflksdjlskdjflksd', ' 中文版本中文欧式的回复就卡死的回复可见阿萨德飞回家收卡的分哈克说地方很少见的开发挥洒的f收到发货就死定了', 'FELMINGTON', '', '', 'title ne', '中文', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `request_no` int(11) NOT NULL AUTO_INCREMENT,
  `request_type` varchar(100) NOT NULL,
  `request_date` date NOT NULL,
  `property_type` varchar(100) NOT NULL,
  `property_no` int(11) NOT NULL,
  `request_email` varchar(100) NOT NULL,
  `request_status` varchar(100) NOT NULL,
  `request_note` text NOT NULL,
  PRIMARY KEY (`request_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_no`, `request_type`, `request_date`, `property_type`, `property_no`, `request_email`, `request_status`, `request_note`) VALUES
(1, 'question', '2015-01-08', 'rent', 81, 'frankcf329@126.com', 'unfinished', 'this is a notedsffad '),
(2, 'inspection', '2015-01-08', 'rent', 81, 'frankcf329@126.com', 'done', 'this is a notedsffad ads'),
(3, 'question', '2015-01-14', 'rent', 81, '', 'done', ' '),
(4, 'question', '2015-01-14', 'rent', 81, 'dfs', 'done', ' sdfsd'),
(5, 'question', '2015-01-14', 'rent', 81, '', 'done', 'djkasldajs '),
(6, 'question', '2015-01-14', 'rent', 81, '', 'done', ' fd'),
(7, 'question', '2015-01-14', 'rent', 81, '', 'done', ' '),
(8, 'question', '2015-01-14', 'rent', 81, 'ds', 'done', ' dsafdsa'),
(9, 'question', '2015-01-14', 'rent', 81, '', 'done', ' '),
(10, 'question', '2015-01-14', 'buy', 81, '', 'done', ' '),
(11, 'question', '2015-01-15', 'rent', 81, 'frankcf329@gmail.com', 'unresponsive', 'jadklasj\n ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
